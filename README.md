# Boogie2Why3 #
This Repository contains material from my master's thesis titled "Supporting multiple proof engines by translating between intermediate verification languages".
The contents are:

- boogie2why3: The source code of our translator, which takes Boogie programs and translates them into WhyMl programs, to be verified by the Why3 system.
- report: the written report. Includes the detailed translation scheme from Boogie to Why3.
- Evaluation: All the Boogie program benchmarks mentioned in the report, including some manual translations.
- BoogieAmp: A Boogie parser and AST generator written in Java. The original project can be found at: https://github.com/martinschaef/boogieamp We made some slight modifications to fit the needs of this project.
- boogie2why3.jar: The executable jar file to perform a translation.

To perform a translation, download boogie2why3.jar, and execute the following command (requires java runtime to be installed):
```
java -jar inputFile.bpl outputfile.mlw
```

The translated program is written to the output file.

The boogie2why3 source includes boogieamp as a library, and can be compiled using java version 8. No further libraries are needed.

For questions and comments you can contact me at michael.ameri@gmail.com

License: GPLv2