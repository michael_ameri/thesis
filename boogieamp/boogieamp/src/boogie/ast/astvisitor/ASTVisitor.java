package boogie.ast.astvisitor;


import boogie.ast.*;
import boogie.ast.asttypes.ArrayAstType;
import boogie.ast.asttypes.NamedAstType;
import boogie.ast.asttypes.PrimitiveAstType;
import boogie.ast.declaration.*;
import boogie.ast.expression.*;
import boogie.ast.expression.literal.*;
import boogie.ast.specification.EnsuresSpecification;
import boogie.ast.specification.LoopInvariantSpecification;
import boogie.ast.specification.ModifiesSpecification;
import boogie.ast.specification.RequiresSpecification;
import boogie.ast.statement.*;

public interface ASTVisitor {
    public void visit(BitvecLiteral bvl);
    public void visit(BooleanLiteral bl);
    public void visit(IntegerLiteral il);
    public void visit(RealLiteral rl);
    public void visit(StringLiteral sl);
    public void visit(ArrayAccessExpression aae);
    public void visit(ArrayStoreExpression ase);
    public void visit(BinaryExpression be);
    public void visit(BitVectorAccessExpression bitVectorAccessExpression);
    public void visit(CodeExpression codeExpression);
    public void visit(FunctionApplication functionApplication);
    public void visit(IdentifierExpression identifierExpression);
    public void visit(IfThenElseExpression ifThenElseExpression);
    public void visit(QuantifierExpression quantifierExpression);
    public void visit(UnaryExpression unaryExpression);
    public void visit(WildcardExpression wildcardExpression);
    public void visit(EnsuresSpecification ensuresSpecification);
    public void visit(LoopInvariantSpecification loopInvariantSpecification);
    public void visit(ModifiesSpecification modifiesSpecification);
    public void visit(RequiresSpecification requiresSpecification);
    public void visit(AssertStatement assertStatement);
    public void visit(AssignmentStatement assignmentStatement);
    public void visit(AssumeStatement assumeStatement);
    public void visit(BreakStatement breakStatement);
    public void visit(CallStatement callStatement);
    public void visit(GotoStatement gotoStatement);
    public void visit(HavocStatement havocStatement);
    public void visit(IfStatement ifStatement);
    public void visit(Label label);
    public void visit(ParallelCall parallelCall);
    public void visit(ReturnStatement returnStatement);
    public void visit(WhileStatement whileStatement);
    public void visit(YieldStatement yieldStatement);
    public void visit(ArrayLHS arrayLHS);
    public void visit(Body body);
    public void visit(NamedAttribute namedAttribute);
    public void visit(ParentEdge parentEdge);
    public void visit(Project project);
    public void visit(Trigger trigger);
    public void visit(Unit unit);
    public void visit(VariableLHS variableLHS);
    public void visit(VarList varList);
    public void visit(Axiom axiom);
    public void visit(ConstDeclaration constDeclaration);
    public void visit(FunctionDeclaration functionDeclaration);
    public void visit(Implementation implementation);
    public void visit(ProcedureDeclaration procedureDeclaration);
    public void visit(TypeDeclaration typeDeclaration);
    public void visit(VariableDeclaration variableDeclaration);
    public void visit(ArrayAstType arrayAstType);
    public void visit(NamedAstType namedAstType);
    public void visit(PrimitiveAstType primitiveAstType);


}
