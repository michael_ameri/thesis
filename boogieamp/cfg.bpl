type real; 
; 
; 
; 
; 
procedure realPower()  returns (res : real)
ensures (res == 8.0);
	root:
		res := (2.0 ** 3.0);
		goto exit;
	exit:
		return;


