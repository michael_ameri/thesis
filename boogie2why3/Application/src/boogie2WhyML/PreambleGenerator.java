package boogie2WhyML;


import whyML.ast.ImportExport;
import whyML.ast.Label;
import whyML.ast.Param;
import whyML.ast.declerations.*;
import whyML.ast.formula.*;
import whyML.ast.formula.quantifier.FormulaBinder;
import whyML.ast.formula.quantifier.QuantifiedFormula;
import whyML.ast.spec.Postcondition;
import whyML.ast.spec.Precondition;
import whyML.ast.terms.FunctionApplicationTerm;
import whyML.ast.terms.SymbolTerm;
import whyML.ast.types.Type;
import whyML.ast.types.TypeSymbol;
import whyML.ast.types.TypeVariable;

import java.util.LinkedList;
import java.util.List;

public class PreambleGenerator {

    /**
     * main method to generate a full preamble
     * @return list of declarations which represent a preamble. each element of the list is a new line.
     */
    public static List<Declaration> generatePreamble(){
        List<Declaration> resultList = new LinkedList<Declaration>();
        resultList.add(new Comment("Preamble Start"));
        generateImports(resultList);
        generateHavocs(resultList);
        generatePartialOrderSymbol(resultList);
        generateExceptionDeclarations(resultList);
        generateIntRealConversionFunctions(resultList);
        resultList.add(new Empty());
        resultList.add(new Comment("Preamble End"));
        resultList.add(new Empty());
        return resultList;
    }

    /**
     *
     * @param resultList is filled with generated functions which belong to the preamble
     */
    private static void generateIntRealConversionFunctions(List<Declaration> resultList){
        List<Param> realInputs = new LinkedList<Param>();
        realInputs.add(new Param("_x", new TypeSymbol("int")));
        FunctionApplicationTerm realTerm = new FunctionApplicationTerm("from_int", new SymbolTerm("_x"));
        resultList.add(new FunctionDeclaration("real",realInputs,realTerm,new TypeSymbol("real"),false));

        List<Param> intInputs = new LinkedList<Param>();
        intInputs.add(new Param("_x", new TypeSymbol("real")));
        FunctionApplicationTerm intTerm = new FunctionApplicationTerm("floor", new SymbolTerm("_x"));
        resultList.add(new FunctionDeclaration("int",intInputs,intTerm,new TypeSymbol("int"),false));
    }

    /**
     *
     * @param resultList is filled with exceptions which need to be in the preamble.
     */
    private static void generateExceptionDeclarations(List<Declaration> resultList){
        resultList.add(new ExceptionDeclaration("Return"));
        resultList.add(new ExceptionDeclaration("Break"));
        resultList.add(new Empty());
    }

    /**
     *
     * @param resultList filled with statements to axiomatize partial order symbol
     */
    private static void generatePartialOrderSymbol(List<Declaration> resultList){
        resultList.add(new Comment("define and axiomatize boogie's partial order operator."));
        List<Param> partialOperatorParams = new LinkedList<Param>();
        partialOperatorParams.add(new Param("_x", new TypeVariable("a")));
        partialOperatorParams.add(new Param("_y", new TypeVariable("a")));
        resultList.add(new Predicate("<:",partialOperatorParams, true));

        //reflexive axiom
        resultList.add(new Comment("reflexive"));
        List<FormulaBinder> bindersReflexive = new LinkedList<FormulaBinder>();
        String reflexiveVar = "_a";
        bindersReflexive.add(new FormulaBinder(reflexiveVar, new TypeVariable("a")));
        Formula reflexiveFormula = new InfixFormula(new SymbolTerm(reflexiveVar),"<:", new SymbolTerm(reflexiveVar));
        QuantifiedFormula qf = new QuantifiedFormula(true, bindersReflexive, reflexiveFormula);
        Axiom a = new Axiom("ReflexivePartialOrder", qf);
        resultList.add(a);

        //transitive axiom
        resultList.add(new Comment("transitive"));
        List<FormulaBinder> bindersTransitive = new LinkedList<FormulaBinder>();
        List<String> transitiveVars = new LinkedList<String>();
        transitiveVars.add("_a");
        transitiveVars.add("_b");
        transitiveVars.add("_c");
        bindersTransitive.add(new FormulaBinder(transitiveVars, new TypeVariable("a")));

        Formula implicatee = new InfixFormula(new SymbolTerm(transitiveVars.get(0)),"<:",new SymbolTerm(transitiveVars.get(2)));
        Formula implicator1 = new InfixFormula(new SymbolTerm(transitiveVars.get(0)),"<:",new SymbolTerm(transitiveVars.get(1)));
        Formula implicator2 = new InfixFormula(new SymbolTerm(transitiveVars.get(1)),"<:",new SymbolTerm(transitiveVars.get(2)));
        Formula implicator = new BinaryFormula(implicator1,"&&",implicator2);

        Formula transitiveFormula = new BinaryFormula(implicator, "->", implicatee);
        QuantifiedFormula qfTransitive = new QuantifiedFormula(true, bindersTransitive, transitiveFormula);
        Axiom transitiveAxiom = new Axiom("TransitivePartialOrder", qfTransitive);
        resultList.add(transitiveAxiom);

        //antisymmetric axiom
        resultList.add(new Comment("antisymmetric"));
        List<FormulaBinder> bindersAS = new LinkedList<FormulaBinder>();
        List<String> antiSymVars = new LinkedList<String>();
        antiSymVars.add("_a");
        antiSymVars.add("_b");
        bindersAS.add(new FormulaBinder(antiSymVars,new TypeVariable("a")));

        Formula implicateeAS = new InfixFormula(new SymbolTerm(antiSymVars.get(0)),"=",new SymbolTerm(antiSymVars.get(1)));
        Formula implicatorAS1 = new InfixFormula(new SymbolTerm(antiSymVars.get(0)),"<:",new SymbolTerm(antiSymVars.get(1)));
        Formula implicatorAS2 = new InfixFormula(new SymbolTerm(antiSymVars.get(1)),"<:",new SymbolTerm(antiSymVars.get(0)));
        Formula implicatorAS = new BinaryFormula(implicatorAS1, "&&", implicatorAS2);

        Formula antiSymFormula = new BinaryFormula(implicatorAS,"->",implicateeAS);
        QuantifiedFormula qfAS = new QuantifiedFormula(true,bindersAS,antiSymFormula);
        Axiom antiSymAxiom = new Axiom("AntiymmetricPartialorder", qfAS);
        resultList.add(antiSymAxiom);
        resultList.add(new Empty());
    }

    /**
     *
     * @param resultList filled with statements of havoc functions
     */
    private static void generateHavocs(List<Declaration> resultList){
        List<Param> havocParams = new LinkedList<Param>();
        havocParams.add(new Param("_x",false,new Label(false),new TypeVariable("a")));
        resultList.add(new ValAbstractFunction("havoc", havocParams,new TypeVariable("a")));
        //resultList.add(new ValAbstractFunction("havoc_int",  new TypeSymbol("int")));
        //resultList.add(new ValAbstractFunction("havoc_real",new TypeSymbol("real")));
        resultList.add(new ValAbstractFunction("havoc_bool", new TypeSymbol("bool")));
        List<Param> havocMapParams = new LinkedList<Param>();
        havocMapParams.add(new Param("_x",new TypeVariable("a")));
        havocMapParams.add(new Param("_y",new TypeVariable("b")));
        List<Type> havocMapReturnSubtypes = new LinkedList<Type>();
        havocMapReturnSubtypes.add(new TypeVariable("a"));
        havocMapReturnSubtypes.add(new TypeVariable("b"));
    }

    /**
     *
     * @param resultList is filled with all necessary import statements.
     */
    private static void generateImports(List<Declaration> resultList){
        resultList.add(new Use(ImportExport.IMPORT, "bool.Bool"));
        resultList.add(new Use(ImportExport.IMPORT, "map.Map"));
        resultList.add(new Use(ImportExport.IMPORT, "ref.Ref"));
        resultList.add(new Use(ImportExport.IMPORT, "int.Int"));
        resultList.add(new Use(ImportExport.IMPORT, "int.EuclideanDivision"));
        resultList.add(new Use(ImportExport.IMPORT, "real.RealInfix"));
        resultList.add(new Comment("int -> real"));
        resultList.add(new Use(ImportExport.IMPORT, "real.FromInt"));
        resultList.add(new Comment("real -> int"));
        resultList.add(new Use(ImportExport.IMPORT, "real.Truncate"));
        resultList.add(new Use(ImportExport.IMPORT, "real.PowerReal"));
        resultList.add(new Empty());
    }
}
