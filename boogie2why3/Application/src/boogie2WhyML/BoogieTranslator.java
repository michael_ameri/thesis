package boogie2WhyML;


import boogie.ProgramFactory;
import boogie.ast.*;
import boogie.ast.Trigger;
import boogie.ast.asttypes.ASTType;
import boogie.ast.asttypes.ArrayAstType;
import boogie.ast.asttypes.NamedAstType;
import boogie.ast.asttypes.PrimitiveAstType;
import boogie.ast.astvisitor.ASTVisitor;
import boogie.ast.declaration.*;
import boogie.ast.declaration.Axiom;
import boogie.ast.declaration.Declaration;
import boogie.ast.declaration.FunctionDeclaration;
import boogie.ast.declaration.TypeDeclaration;
import boogie.ast.expression.*;
import boogie.ast.expression.Expression;
import boogie.ast.expression.literal.*;
import boogie.ast.specification.*;
import boogie.ast.statement.*;
import boogie.ast.statement.Label;
import boogie.enums.BinaryOperator;
import boogie.enums.UnaryOperator;
import boogie.type.BoogieType;
import boogie2WhyML.Helper.*;
import boogie2WhyML.Helper.CallForall.CallForallReplacementVisitor;
import boogie2WhyML.Helper.VariableTypeParam.VariableTypeEnumerator;
import boogie2WhyML.Helper.VariableTypeParam.VariableWithTypeParamFinder;
import javafx.util.Pair;
import typechecker.TypeChecker;
import util.Log;
import whyML.ast.*;
import whyML.ast.ASTNode;
import whyML.ast.declerations.*;
import whyML.ast.declerations.letdeclaration.LetDeclaration;
import whyML.ast.expression.*;
import whyML.ast.formula.*;
import whyML.ast.formula.quantifier.*;
import whyML.ast.pattern.Pattern;
import whyML.ast.pattern.TuplePattern;
import whyML.ast.pattern.VariablePattern;
import whyML.ast.spec.*;
import whyML.ast.terms.*;
import whyML.ast.types.*;

import java.util.*;
import java.util.stream.Collectors;

public class BoogieTranslator implements ASTVisitor {

    public enum FormulaTermExpression{
        FORMULA,
        TERM,
        EXPRESSION
    }

    //used for the child which is called to determine if it should create a formula, term, or expression.
    //Because Boogie's expression/statement can be translated to one of these three, depending on the parents context.
    private FormulaTermExpression formulaTermExpression;

    public void setFormulaTermExpression(FormulaTermExpression formulaTermExpression) {
        this.formulaTermExpression = formulaTermExpression;
    }

    //used for children to store their results.
    private whyML.ast.ASTNode resultNode;

    public whyML.ast.ASTNode getResultNode() {
        return resultNode;
    }

    public void setResultNode(ASTNode resultNode) {
        this.resultNode = resultNode;
    }

    //Type used to not implement ASTNode, so this was used. it could theoretically be replaced by resultNode now.
    private Type resultType;

    private List<whyML.ast.declerations.Declaration> whyMLDeclerations;

    //each axiom needs a unique identifier within WhyML modules.
    private int axiomCounter;

    //Boogie's global and local Variable declarations are the same.
    //in WhyML, we have global variables and local bindings, which results in different translations.
    private boolean isGlobalVariableDeclaration;

    //contains a map from global variable names to the where-clause formula. Can be used to create procedure preamble.
    private Map<String,Formula> globalWhereClauses;
    private ContractGenerator contractGenerator;
    private ConstantAxiomGenerator constantAxiomGenerator;

    /**
     * default initialization
     */
    public BoogieTranslator(){
        this.whyMLDeclerations = new LinkedList<whyML.ast.declerations.Declaration>();
        this.resultNode = null;
        this.formulaTermExpression = null;
        this.axiomCounter = 0;
        this.typeParams = new LinkedList<String>();
        this.procedureTypeParams = new LinkedList<>();
        this.isGlobalVariableDeclaration = true;
        this.globalWhereClauses = new HashMap<String, Formula>();
    }


    /**
     * collects all implementations of a procedure.
     */
    private Map<String,List<Body>> allBodiesOfProcedures = null;

    /**
     *
     * @param procedureName
     * @return list of implementations for procedure called procedureName
     */
    private List<Body> getBodiesOfProcedure(String procedureName){
        return allBodiesOfProcedures.get(procedureName);
    }

    /**
     * default translation. attaches preamble, reorders, but does not rename idents.
     * @param pf
     * @return
     */
    public List<whyML.ast.declerations.Declaration> translate(ProgramFactory pf){
        return translate(pf, true, false, true);
    }

    /**
     * translate the declarations from boogie to whyML.
     * @param pf
     * @return a list of whyML declarations, semantically equivalent to the Boogie declarations.
     */
    public List<whyML.ast.declerations.Declaration> translate(ProgramFactory pf, Boolean attachPreamble, Boolean renameIdents, Boolean reorder){
        BoogieIdentRenaming boogieIdentRenaming = new BoogieIdentRenaming();
        boogieIdentRenaming.replaceIdents(pf.getGlobalDeclarations());
        ProcedureBodyFactory procedureBodyFactory =  new ProcedureBodyFactory();
        allBodiesOfProcedures = procedureBodyFactory.modifyProcedureDeclarations(pf);
        //replace callforall statements by assume statements.
        new CallForallReplacementVisitor(pf);
        //in case we want to output boogie file with replaced call-forall statements, uncomment the following line.
        //could also add option to input parameters.
//      pf.toFile("D:\\out.bpl");
        Log.info("Run the typechecker");
        TypeChecker tc = new TypeChecker(pf.getASTRootWithoutModfiesClause());
        Log.info("Typechecker Done.");

        /*
        //TODO in future, this could be used as a start to translate maps with type params.
        VariableWithTypeParamFinder variableWithTypeParamFinder = new VariableWithTypeParamFinder(pf);
        List<String> variablesWithTypeParams = variableWithTypeParamFinder.getResultVariableNames();
        VariableTypeEnumerator variableTypeEnumerator = new VariableTypeEnumerator(pf,variablesWithTypeParams,new LinkedList<>());
        */

        contractGenerator = new ContractGenerator(pf);
        //this is needed to generate whereClauses correctly later. (because IdentifierExpression representing a global variable
        //needs to be translated to x.contents)
        fillGlobalVariableIdentifiers(pf);

        //ConstantAxiomGenerator is two fold: it generates some axioms itself, and then it has some handy functions which can be used in
        //the vistor of ConstDeclaration to generate the other necessary axioms.
        constantAxiomGenerator = new ConstantAxiomGenerator(pf);
        whyMLDeclerations.addAll(constantAxiomGenerator.getGeneratedAxioms());

        //first need to visit only type declarations, because some methods depend on this information.
        for(Declaration d: pf.getASTRoot().getDeclarations()){
            if(d instanceof TypeDeclaration){
                d.accept(this);
            }
        }

        //visit all other declarations.
        for(boogie.ast.declaration.Declaration d: pf.getASTRoot().getDeclarations()){
            if(d instanceof TypeDeclaration){
                continue;
            }
            if(d instanceof VariableDeclaration){
                isGlobalVariableDeclaration = true;
            }else {
                isGlobalVariableDeclaration = false;
            }
            d.accept(this);
        }

        //rename, reorder and attach preamble if wanted.
        if(renameIdents){
            WhyMLIdentRenaming whyMLIdentRenaming = new WhyMLIdentRenaming();
            whyMLIdentRenaming.renameAccordingToWhyMLSyntax(whyMLDeclerations);
        }
        if(reorder && attachPreamble){
            DeclarationReordering declarationReordering = new DeclarationReordering(PreambleGenerator.generatePreamble(),whyMLDeclerations, new LinkedList<>());
            whyMLDeclerations = declarationReordering.getOrderedDeclarations();
        }else if(reorder && !attachPreamble){
            DeclarationReordering declarationReordering = new DeclarationReordering(new LinkedList<>(),whyMLDeclerations, new LinkedList<>());
            whyMLDeclerations = declarationReordering.getOrderedDeclarations();
        }
        return whyMLDeclerations;
    }


    /**
     * finds all global variables and stores the identifiers.
     * @param pf
     */
    private void fillGlobalVariableIdentifiers(ProgramFactory pf){
        for(boogie.ast.declaration.Declaration d: pf.getASTRoot().getDeclarations()){
            if(d instanceof VariableDeclaration){
                VariableDeclaration variableDeclaration = (VariableDeclaration) d;
                isGlobalVariableDeclaration = true;
                for(VarList varList: variableDeclaration.getVariables()){
                    for(int i = 0; i < varList.getIdentifiers().length; ++i){
                        globalVariableIdentifiers.add(varList.getIdentifiers()[i]);
                    }
                }
            }
        }
    }


    public List<whyML.ast.declerations.Declaration> getWhyMLDeclerations() {
        return whyMLDeclerations;
    }


    /**
     * Tranlsate boolean literal, which is either true or false.
     * @param booleanLiteral
     */
    @Override
    public void visit(BooleanLiteral booleanLiteral) {
        if(formulaTermExpression == FormulaTermExpression.FORMULA){
            if(booleanLiteral.getValue()){
                resultNode = new TrueFormula();
            }else{
                resultNode = new FalseFormula();
            }
        }else if(formulaTermExpression == FormulaTermExpression.TERM){
            //e.g. for function application
            if(booleanLiteral.getValue()){
                resultNode = new SymbolTerm("true");
            }else{
                resultNode =  new SymbolTerm("false");
            }
        }else{
            if(booleanLiteral.getValue()){
                resultNode = new SymbolExpression("true");
            }else{
                resultNode = new SymbolExpression("false");
            }
        }
    }

    @Override
    public void visit(BitvecLiteral bitvecLiteral) {
        //TODO
        throw new RuntimeException("implement bitvecliteral");
    }

    /**
     * translate integer literal.
     * @param integerLiteral
     */
    @Override
    public void visit(IntegerLiteral integerLiteral) {
        if(formulaTermExpression == FormulaTermExpression.FORMULA){
            throw new RuntimeException("visit IntegerLiteral cannot create a formula.");
        }else if(formulaTermExpression == FormulaTermExpression.TERM){
            resultNode = new IntegerTerm(integerLiteral.getValue());
        }else{//FormulaTermExpression.EXPRESSION
            resultNode = new IntegerExpression(integerLiteral.getValue());
        }
    }

    /**
     * translate real value.
     * @param realLiteral to be translated.
     */
    @Override
    public void visit(RealLiteral realLiteral) {
        if(formulaTermExpression == FormulaTermExpression.FORMULA){
            //ERROR, throw exception, should never be the case.
            throw new RuntimeException("visit RealLiteral cannot create a formula.");
        }else if(formulaTermExpression == FormulaTermExpression.TERM){
            resultNode = new RealTerm(realLiteral.getValue());
        }else{//FormulaTermExpression.EXPRESSION
            resultNode = new RealExpression(realLiteral.getValue());
        }
    }

    @Override
    public void visit(StringLiteral stringLiteral) {
        //is only used for attributes, which are not translated for now. might add support in future for specific attributes.
    }

    /**
     *boogie: a[i]
     *whyML: (get a i)
     * @param arrayAccessExpression
     */
    @Override
    public void visit(ArrayAccessExpression arrayAccessExpression) {
        if(formulaTermExpression == FormulaTermExpression.FORMULA) {
            String predicateName = "get";
            formulaTermExpression = FormulaTermExpression.TERM;
            arrayAccessExpression.getArray().accept(this);
            Term arrayName = (Term)resultNode;

            formulaTermExpression = FormulaTermExpression.TERM;
            if(arrayAccessExpression.getIndices().length == 0){
                Term index = new SymbolTerm("()");
                resultNode = new PredicateApplicationFormula(predicateName,index);
            }else if(arrayAccessExpression.getIndices().length == 1){
                arrayAccessExpression.getIndices()[0].accept(this);
                Term index = (Term) resultNode;
                List<Term> arguments = new LinkedList<>();
                arguments.add(arrayName);
                arguments.add(index);
                resultNode = new PredicateApplicationFormula(predicateName,arguments);
            }else {
                List<Term> termsList = new LinkedList<>();
                //termsList.add(arrayName);
                for (Expression e : arrayAccessExpression.getIndices()) {
                    formulaTermExpression = FormulaTermExpression.TERM;
                    e.accept(this);
                    termsList.add((Term) resultNode);
                }
                Term index = new TupleTerm(termsList);
                List<Term> applicationTerms = new LinkedList<>();
                applicationTerms.add(arrayName);
                applicationTerms.add(index);
                resultNode = new PredicateApplicationFormula(predicateName, applicationTerms);
            }
        }else if(formulaTermExpression == FormulaTermExpression.TERM){
            String functionName = ("get");
            formulaTermExpression = FormulaTermExpression.TERM;
            arrayAccessExpression.getArray().accept(this);
            Term arrayName = (Term)resultNode;

            formulaTermExpression = FormulaTermExpression.TERM;
            if(arrayAccessExpression.getIndices().length == 0){
                Term index = new SymbolTerm("()");
                resultNode = new FunctionApplicationTerm(functionName,index);
            }else if(arrayAccessExpression.getIndices().length == 1){
                arrayAccessExpression.getIndices()[0].accept(this);
                Term index = (Term) resultNode;
                List<Term> arguments = new LinkedList<>();
                arguments.add(arrayName);
                arguments.add(index);
                resultNode = new FunctionApplicationTerm(functionName,arguments);
            }else{
                List<Term> termsList = new LinkedList<>();
                //termsList.add(arrayName);
                for(Expression e: arrayAccessExpression.getIndices()){
                    formulaTermExpression = FormulaTermExpression.TERM;
                    e.accept(this);
                    termsList.add((Term)resultNode);
                }
                Term index = new TupleTerm(termsList);
                List<Term> applicationTerms = new LinkedList<>();
                applicationTerms.add(arrayName);
                applicationTerms.add(index);
                resultNode = new FunctionApplicationTerm(functionName,applicationTerms);
            }
        }else{//EXPRESSION
            whyML.ast.expression.Expression functionName = new SymbolExpression("get");
            formulaTermExpression = FormulaTermExpression.EXPRESSION;
            arrayAccessExpression.getArray().accept(this);
            whyML.ast.expression.Expression arrayExpression = (whyML.ast.expression.Expression)resultNode;
            whyML.ast.expression.Expression index;
            formulaTermExpression = FormulaTermExpression.EXPRESSION;
            if(arrayAccessExpression.getIndices().length == 0){
                index = new SymbolExpression("()");
            }else if(arrayAccessExpression.getIndices().length == 1){
                arrayAccessExpression.getIndices()[0].accept(this);
                index = (whyML.ast.expression.Expression)resultNode;
            }else{
                List<whyML.ast.expression.Expression> tupleList = new LinkedList<>();
                for(Expression e:arrayAccessExpression.getIndices()){
                    formulaTermExpression = FormulaTermExpression.EXPRESSION;
                    e.accept(this);
                    tupleList.add((whyML.ast.expression.Expression)resultNode);
                }
                index = new TupleExpression(tupleList);
            }
            List<whyML.ast.expression.Expression> functionArguments = new LinkedList<>();
            functionArguments.add(arrayExpression);
            functionArguments.add(index);
            resultNode = new FunctionApplicationExpression(functionName,functionArguments);

        }
    }


    /**
     * if there are multiple indices, a tuple is created. If there are no indices, () is created.
     * Result is stored in resultNode. creates a formula, term or expression.
     *
     * @param indices should not be null.
     * @param fte
     */
    private void translateArrayIndices(Expression[] indices, FormulaTermExpression fte){
        if(fte == FormulaTermExpression.EXPRESSION){
            if(indices.length == 0){
                resultNode = new SymbolExpression("()");
            }else if(indices.length == 1){
                //automatically store result in resultNode
                formulaTermExpression = fte;
                indices[0].accept(this);
            }else {//length >= 2
                List<whyML.ast.expression.Expression> tupleExpressions = new LinkedList<>();
                for(Expression e : indices){
                    formulaTermExpression = fte;
                    e.accept(this);
                    tupleExpressions.add((whyML.ast.expression.Expression)resultNode);
                }
                resultNode = new TupleExpression(tupleExpressions);
            }
        }else if(fte == FormulaTermExpression.FORMULA){
            throw new RuntimeException("indices cannot be a formula.");
        }else if(fte == FormulaTermExpression.TERM){
            if(indices.length == 0){
                resultNode = new SymbolTerm("()");
            }else if(indices.length == 1){
                //automatically store result in resultNode
                formulaTermExpression = fte;
                indices[0].accept(this);
            }else {//length >= 2
                List<Term> tupleTerms = new LinkedList<>();
                for(Expression e : indices){
                    formulaTermExpression = fte;
                    e.accept(this);
                    tupleTerms.add((Term) resultNode);
                }
                resultNode = new TupleTerm(tupleTerms);
            }
        }else{
            throw new RuntimeException("unexpected formula term expression.");
        }
    }

    /**
     * translation of the form:
     * boogie:
     *array[index1, ..., indexn := value]
     * whyML:
     *set (array) (index1, ..., indexn) (value)
     * @param arrayStoreExpression
     */
    @Override
    public void visit(ArrayStoreExpression arrayStoreExpression) {
        String storeFunctionName = "set";
        if(formulaTermExpression == FormulaTermExpression.EXPRESSION){
            whyML.ast.expression.Expression setExpression = new SymbolExpression(storeFunctionName);
            formulaTermExpression = FormulaTermExpression.EXPRESSION;
            arrayStoreExpression.getArray().accept(this);
            whyML.ast.expression.Expression arrayArgumentExpresion = (whyML.ast.expression.Expression)resultNode;
            formulaTermExpression = FormulaTermExpression.EXPRESSION;
            arrayStoreExpression.getValue().accept(this);
            whyML.ast.expression.Expression valueArgumentExpression = (whyML.ast.expression.Expression)resultNode;

            translateArrayIndices(arrayStoreExpression.getIndices(), FormulaTermExpression.EXPRESSION);
            whyML.ast.expression.Expression indexArgumentExpression = (whyML.ast.expression.Expression)resultNode;

            List<whyML.ast.expression.Expression> arguments = new LinkedList<>();
            arguments.add(arrayArgumentExpresion);
            arguments.add(indexArgumentExpression);
            arguments.add(valueArgumentExpression);

            resultNode = new FunctionApplicationExpression(setExpression, arguments);
        }else if(formulaTermExpression == FormulaTermExpression.TERM || formulaTermExpression == FormulaTermExpression.FORMULA){
            formulaTermExpression = FormulaTermExpression.TERM;
            arrayStoreExpression.getArray().accept(this);
            Term arrayArgumentTerm = (Term)resultNode;
            formulaTermExpression = FormulaTermExpression.TERM;
            arrayStoreExpression.getValue().accept(this);
            Term valueArgument = (Term)resultNode;
            translateArrayIndices(arrayStoreExpression.getIndices(), FormulaTermExpression.TERM);
            Term indexArgument = (Term)resultNode;

            List<Term> arguments = new LinkedList<>();
            arguments.add(arrayArgumentTerm);
            arguments.add(indexArgument);
            arguments.add(valueArgument);

            if(formulaTermExpression == FormulaTermExpression.TERM ){
                resultNode = new FunctionApplicationTerm(storeFunctionName, arguments);
            }else{//Formula
                resultNode = new PredicateApplicationFormula(storeFunctionName,arguments);
            }
        }else{
            throw new RuntimeException("expression, term, formula has been extended.");
        }
    }

    /**
     * returns true if op is one of: <->, ->, &&, ||
     * @param op
     * @return
     */
    private boolean isBinaryFormulaOperator(BinaryOperator op){
        return BinaryOperator.LOGICIFF.equals(op) || BinaryOperator.LOGICIMPLIES.equals(op)
                ||  BinaryOperator.LOGICAND.equals(op) ||  BinaryOperator.LOGICOR.equals(op);
    }

    private boolean isBinaryComparatorOperator(BinaryOperator op){
        return BinaryOperator.COMPEQ.equals(op) || BinaryOperator.COMPPO.equals(op) ||
                BinaryOperator.COMPNEQ.equals(op) || BinaryOperator.COMPLT.equals(op) ||
                BinaryOperator.COMPLEQ.equals(op) || BinaryOperator.COMPGT.equals(op) ||
                BinaryOperator.COMPGEQ.equals(op);
    }



    /**
     * translate logical boogie operator
     * @param op
     * @return
     */
    private String boogieLogicalOperatorToWhyMLstring(BinaryOperator op){
        switch (op){
            case LOGICAND:
                return "&&";
            case LOGICIFF:
                return "<->";
            case LOGICIMPLIES:
                return "->";
            case LOGICOR:
                return "||";
            default:
                throw new RuntimeException("invalid binary logical operator: "+op.toString());
        }
    }

    private String boogieComparatorOperatorToWhyMLstring(BinaryOperator op, boolean isRealType){
        String res;
        switch (op){
            case COMPEQ:
                res = "=";
                break;
            case COMPGEQ:
                res = ">=";
                break;
            case COMPGT:
                res = ">";
                break;
            case COMPLEQ:
                res = "<=";
                break;
            case COMPLT:
                res = "<";
                break;
            case COMPNEQ:
                res = "<>";
                break;
            case COMPPO:
                res = "<:";
                break;
            default:
                throw new RuntimeException("Invalid Comparator binary operator: "+op.toString());
        }
        //for real, we need <. <=. >. and >=. (but =, <> and <: stay the same.
        if(isRealType){
            if(op != BinaryOperator.COMPPO && op != BinaryOperator.COMPEQ && op != BinaryOperator.COMPNEQ){
                res = res + ".";
            }
        }
        return res;
    }


    /**
     * returns the BOOGIE representation of the operator.
     * @param op
     * @return
     */
    private String arithmeticOperatorToString(BinaryOperator op){
        switch (op){
            case ARITHMINUS:
                return "-";
            case ARITHMUL:
                return "*";
            case ARITHDIVINT:
                return "div";
            case ARITHDIVREAL:
                return "/";
            case ARITHEXP:
                return "**";
            case ARITHMOD:
                //throw new RuntimeException("mod parsing not implemented yet.");
                return "mod";
            case ARITHPLUS:
                return "+";
            default:
                throw new RuntimeException("Non arithmetic operator: "+op);

        }
    }


    /**
     * translates a binary arithmetic expression, stores the result in resultNode. type is Term or Expression, depending on formulaTermExpression.
     * @param leftExpression
     * @param op
     * @param rightExpression
     */
    private void translateBinaryArithmeticExpression(Expression leftExpression, BinaryOperator op, Expression rightExpression){
        if(formulaTermExpression == FormulaTermExpression.TERM){
            //TODO BVconcat operator
            if(BinaryOperator.ARITHDIVINT.equals(op) || BinaryOperator.ARITHMOD.equals(op) ){
                formulaTermExpression =FormulaTermExpression.TERM;
                leftExpression.accept(this);
                Term leftTerm = (Term) resultNode;
                formulaTermExpression = FormulaTermExpression.TERM;
                rightExpression.accept(this);
                Term rightTerm = (Term) resultNode;

                List<Term> args = new LinkedList<>();
                args.add(leftTerm);
                args.add(rightTerm);
                resultNode = new FunctionApplicationTerm(arithmeticOperatorToString(op),args);
            }else if(BinaryOperator.ARITHMINUS.equals(op) ||BinaryOperator.ARITHPLUS.equals(op) || BinaryOperator.ARITHMUL.equals(op)){
                if("int".equals(leftExpression.getType().toString())){
                    if(!("int".equals(rightExpression.getType().toString()))){
                        throw new RuntimeException("mismatched types: "+leftExpression+": "+leftExpression.getType()+" / "
                        + rightExpression+ ": " + rightExpression.getType());
                    }
                    formulaTermExpression =FormulaTermExpression.TERM;
                    leftExpression.accept(this);
                    Term leftTerm = (Term) resultNode;
                    formulaTermExpression = FormulaTermExpression.TERM;
                    rightExpression.accept(this);
                    Term rightTerm = (Term) resultNode;
                    resultNode = new InfixTerm(leftTerm,arithmeticOperatorToString(op),rightTerm);

                }else if("real".equals(leftExpression.getType().toString())){
                    if(!("real".equals(rightExpression.getType().toString()))){
                        throw new RuntimeException("mismatched types: "+leftExpression+": "+leftExpression.getType()+" / "
                                + rightExpression+ ": " + rightExpression.getType());
                    }
                    formulaTermExpression =FormulaTermExpression.TERM;
                    leftExpression.accept(this);
                    Term leftTerm = (Term) resultNode;
                    formulaTermExpression = FormulaTermExpression.TERM;
                    rightExpression.accept(this);
                    Term rightTerm = (Term) resultNode;
                    resultNode = new InfixTerm(leftTerm,arithmeticOperatorToString(op)+".",rightTerm);

                }else{
                    throw new RuntimeException("Invalid expression types for Arithmetic term translation."+leftExpression+": "+leftExpression.getType()+" / "
                            + rightExpression+ ": " + rightExpression.getType());
                }
            }else if(BinaryOperator.ARITHDIVREAL.equals(op) ){
                formulaTermExpression = FormulaTermExpression.TERM;
                leftExpression.accept(this);
                Term leftTerm = (Term)resultNode;
                formulaTermExpression = FormulaTermExpression.TERM;
                rightExpression.accept(this);
                Term rightTerm = (Term)resultNode;
                if("int".equals(leftExpression.getType().toString())){
                    leftTerm = new FunctionApplicationTerm("from_int",leftTerm);
                }
                if("int".equals(rightExpression.getType().toString())){
                    rightTerm = new FunctionApplicationTerm("from_int",rightTerm);
                }
                resultNode = new InfixTerm(leftTerm,arithmeticOperatorToString(op)+".",rightTerm);
            }else if(BinaryOperator.ARITHEXP.equals(op)){
                formulaTermExpression = FormulaTermExpression.TERM;
                leftExpression.accept(this);
                Term leftTerm = (Term) resultNode;
                formulaTermExpression = FormulaTermExpression.TERM;
                rightExpression.accept(this);
                Term rightTerm  = (Term) resultNode;
                List<Term> args = new LinkedList<>();
                args.add(leftTerm);
                args.add(rightTerm);
                resultNode = new FunctionApplicationTerm("pow",args);
            }else{
                throw new RuntimeException("Unknown binary operator to translate: "+op);
            }
        }else if(formulaTermExpression == FormulaTermExpression.EXPRESSION){//it is an expression. Formula is not valid.
            formulaTermExpression = FormulaTermExpression.EXPRESSION;
            leftExpression.accept(this);
            whyML.ast.expression.Expression leftWhyML = (whyML.ast.expression.Expression)resultNode;
            formulaTermExpression = FormulaTermExpression.EXPRESSION;
            rightExpression.accept(this);
            whyML.ast.expression.Expression rightWhyML = (whyML.ast.expression.Expression)resultNode;
            if(BinaryOperator.ARITHDIVINT.equals(op) || BinaryOperator.ARITHMOD.equals(op) ){
                List<whyML.ast.expression.Expression> args = new LinkedList<>();
                args.add(leftWhyML);
                args.add(rightWhyML);
                resultNode = new FunctionApplicationExpression(new SymbolExpression(arithmeticOperatorToString(op)),args);
            }else if(BinaryOperator.ARITHMINUS.equals(op) ||BinaryOperator.ARITHPLUS.equals(op) || BinaryOperator.ARITHMUL.equals(op)){
                //we can assume left and right have the same type. Otherwise it is not legal boogie syntax.
                if("real".equals(leftExpression.getType().toString())){
                    resultNode = new InfixExpression(leftWhyML,arithmeticOperatorToString(op)+".",rightWhyML);
                }else if("int".equals(leftExpression.getType().toString())){
                    resultNode = new InfixExpression(leftWhyML,arithmeticOperatorToString(op),rightWhyML);
                }else{
                    throw new RuntimeException("Type of arithmetic Binary expression should be int or real");
                }
            }else if(BinaryOperator.ARITHDIVREAL.equals(op) ){
                if("int".equals(leftExpression.getType().toString())){
                    leftWhyML = new FunctionApplicationExpression(new SymbolExpression("from_int"),leftWhyML);
                }
                if("int".equals(rightExpression.getType().toString())){
                    rightWhyML = new FunctionApplicationExpression(new SymbolExpression("from_int"),rightWhyML);
                }
                resultNode = new InfixExpression(leftWhyML,"/.",rightWhyML);
            }else if(BinaryOperator.ARITHEXP.equals(op)) {
                List<whyML.ast.expression.Expression> args = new LinkedList<>();
                args.add(leftWhyML);
                args.add(rightWhyML);
                resultNode = new FunctionApplicationExpression(new SymbolExpression("pow"), args);
            }else{
                throw new RuntimeException("Unknown binary operator to translate: "+op);
            }
        }else {
            throw new RuntimeException("cannot create binary formula with operator: "+op.toString());
        }
    }


    @Override
    public void visit(BinaryExpression binaryExpression) {
        //TODO BVconcat operator
        if(formulaTermExpression == FormulaTermExpression.FORMULA){
            if(isBinaryFormulaOperator(binaryExpression.getOperator())){
                formulaTermExpression = FormulaTermExpression.FORMULA;
                binaryExpression.getLeft().accept(this);
                Formula leftFormula = (Formula) resultNode;
                formulaTermExpression = FormulaTermExpression.FORMULA;
                binaryExpression.getRight().accept(this);
                Formula rightFormula = (Formula) resultNode;
                resultNode = new BinaryFormula(leftFormula, boogieLogicalOperatorToWhyMLstring(binaryExpression.getOperator()),rightFormula);
            }else{//formula = term infixop term
                formulaTermExpression = FormulaTermExpression.TERM;
                binaryExpression.getLeft().accept(this);
                Term leftTerm = (Term) resultNode;
                formulaTermExpression = FormulaTermExpression.TERM;
                binaryExpression.getRight().accept(this);
                Term rightTerm = (Term) resultNode;
                boolean isRealType = BoogieType.realType.equals(binaryExpression.getLeft().getType());
                resultNode = new InfixFormula(leftTerm, boogieComparatorOperatorToWhyMLstring(binaryExpression.getOperator(), isRealType),rightTerm);
                if(!isBinaryComparatorOperator(binaryExpression.getOperator())){
                    throw new RuntimeException("cannot build a formula with this operator:" +binaryExpression.getOperator().toString());
                }
            }
            formulaTermExpression = FormulaTermExpression.FORMULA;//set it back to what is was originally. cannot count on this to always be done.
        }else if(formulaTermExpression == FormulaTermExpression.TERM){
            if(isBinaryComparatorOperator(binaryExpression.getOperator())){
                formulaTermExpression = FormulaTermExpression.TERM;
                binaryExpression.getLeft().accept(this);
                Term leftTerm = (Term) resultNode;
                formulaTermExpression = FormulaTermExpression.TERM;
                binaryExpression.getRight().accept(this);
                Term rightTerm = (Term) resultNode;
                boolean isRealType = BoogieType.realType.equals(binaryExpression.getLeft().getType());
                resultNode = new InfixTerm(leftTerm, boogieComparatorOperatorToWhyMLstring(binaryExpression.getOperator(),isRealType),rightTerm);
            }else if(isBinaryFormulaOperator(binaryExpression.getOperator())) {
                formulaTermExpression = FormulaTermExpression.TERM;
                binaryExpression.getLeft().accept(this);
                Term leftTerm = (Term) resultNode;
                formulaTermExpression = FormulaTermExpression.TERM;
                binaryExpression.getRight().accept(this);
                Term rightTerm = (Term) resultNode;
                resultNode = new InfixTerm(leftTerm,boogieLogicalOperatorToWhyMLstring(binaryExpression.getOperator()),rightTerm);
            }else {//arithmeticoperator
                formulaTermExpression = FormulaTermExpression.TERM;
                translateBinaryArithmeticExpression(binaryExpression.getLeft(),binaryExpression.getOperator(),binaryExpression.getRight());
                //last line automatically sets resultNode.
            }
        }else{//FormulaTermExpression.EXPRESSION
            if(isBinaryComparatorOperator(binaryExpression.getOperator())){
                formulaTermExpression = FormulaTermExpression.EXPRESSION;
                binaryExpression.getLeft().accept(this);
                whyML.ast.expression.Expression leftExpression = (whyML.ast.expression.Expression)resultNode;
                formulaTermExpression = FormulaTermExpression.EXPRESSION;
                binaryExpression.getRight().accept(this);
                whyML.ast.expression.Expression rightExpression = (whyML.ast.expression.Expression)resultNode;
                boolean isRealType = (BoogieType.realType.equals(binaryExpression.getLeft().getType()));
                resultNode = new InfixExpression(leftExpression,boogieComparatorOperatorToWhyMLstring(binaryExpression.getOperator(), isRealType),rightExpression);
                //throw new RuntimeException("Implement logical WhyML binary expression");
            }else if(isBinaryFormulaOperator(binaryExpression.getOperator())){
                formulaTermExpression = FormulaTermExpression.EXPRESSION;
                binaryExpression.getLeft().accept(this);
                whyML.ast.expression.Expression leftExpression = (whyML.ast.expression.Expression)resultNode;
                formulaTermExpression = FormulaTermExpression.EXPRESSION;
                binaryExpression.getRight().accept(this);
                whyML.ast.expression.Expression rightExpression = (whyML.ast.expression.Expression)resultNode;
                resultNode = new InfixExpression(leftExpression,boogieLogicalOperatorToWhyMLstring(binaryExpression.getOperator()),rightExpression);
            }else{
                formulaTermExpression = FormulaTermExpression.EXPRESSION;
                translateBinaryArithmeticExpression(binaryExpression.getLeft(),binaryExpression.getOperator(),binaryExpression.getRight());
            }
        }
    }

    @Override
    public void visit(BitVectorAccessExpression bitVectorAccessExpression) {
        //TODO
    }

    @Override
    public void visit(CodeExpression codeExpression) {
        //TODO
    }

    /**
     * translate function application.
     * @param functionApplication
     */
    @Override
    public void visit(FunctionApplication functionApplication) {
        if(formulaTermExpression == FormulaTermExpression.TERM){
            List<Term> arguments = new LinkedList<Term>();
            for(Expression e: functionApplication.getArguments()){
                formulaTermExpression = FormulaTermExpression.TERM;
                e.accept(this);
                arguments.add((Term)resultNode);
            }
            resultNode = new FunctionApplicationTerm(functionApplication.getIdentifier(),arguments);
        }else if(formulaTermExpression == FormulaTermExpression.EXPRESSION){
            whyML.ast.expression.Expression nameExpression = new SymbolExpression(functionApplication.getIdentifier());
            List<whyML.ast.expression.Expression> arguments = new LinkedList<>();
            for(Expression e: functionApplication.getArguments()){
                formulaTermExpression = FormulaTermExpression.EXPRESSION;
                e.accept(this);
                arguments.add((whyML.ast.expression.Expression)resultNode);
            }
            resultNode = new FunctionApplicationExpression(nameExpression,arguments);
        }else {//formula
            List<Term> arguments = new LinkedList<Term>();
            for(Expression e: functionApplication.getArguments()){
                formulaTermExpression = FormulaTermExpression.TERM;
                e.accept(this);
                arguments.add((Term)resultNode);
            }
            resultNode = new PredicateApplicationFormula(functionApplication.getIdentifier(),arguments);
            //throw new RuntimeException("translate function application to predicate application.");
        }
    }


    //stays the same for the whole Boogie program. Contains the name from Boogie AST
    HashSet<String> globalVariableIdentifiers = new HashSet<String>();

    //changes based on the current procedure. Contains the name from WhyML AST
    HashSet<String> localVariableIdentifiers = new HashSet<String>();

    //changes based on current procedure. Contains name from Boogie AST. With these we want to avoid name clash, but NOT add .contents to the end.
    HashSet<String> localInputParameters = new HashSet<>();

    private String currentProcedureName;

    /**
     * a Boogie local variable creates a global variable, which can be taken as input for havoc to create local binding.
     * @return
     */
    private String globalLocalVariableIdentifierPrefix() {
        return "_GLOBAL_LOCAL_"+currentProcedureName+"_";
    }

    /**
     * name of the locally bound variable has this prefix.
     * @return
     */
    private String localVariableIdentifierPrefix() {
        //no need for change anymore, it is done in BoogieIdentRenaming now.
        //return "_LOCAL_"+currentProcedureName+"_";
        return "";
    }



    @Override
    public void visit(IdentifierExpression identifierExpression) {
        String identifier = identifierExpression.getIdentifier();
        //variables are translated to use "ref" as mutable containers. This step allows us to get the part we want.
        //to be absolutely correct, it should be translated to a fieldaccess term or expression, if it is a global variable.
        if(globalVariableIdentifiers.contains(identifier) || localVariableIdentifiers.contains(identifier)){
            identifier = identifier.trim() +".contents";
        }
        if(localVariableIdentifiers.contains(identifierExpression.getIdentifier())){
            identifier = localVariableIdentifierPrefix()+identifier;
        }
        if(formulaTermExpression == FormulaTermExpression.FORMULA){
            resultNode = new SymbolFormula(identifier);
        }else if(formulaTermExpression == FormulaTermExpression.TERM){
            resultNode = new SymbolTerm(identifier);
        }else{//FormulaTermExpression.EXPRESSION
            resultNode = new SymbolExpression(identifier);
        }
    }


    /**
     * this is only used for functions, which are axiomatized at another point. (see ifstatement visitor for how
     * if-statements within procedures are translated)
     * @param ifThenElseExpression
     */
    @Override
    public void visit(IfThenElseExpression ifThenElseExpression) {
        //do nothing.
        Log.info("IfThenElseExpression is not translated. it is axiomatized instead.");
        //this is for functions.. for procedures ifStatements are used.
        //however functions are axiomatized, so its not really needed.
        //throw new RuntimeException("implement");
    }

    @Override
    public void visit(QuantifierExpression quantifierExpression) {
        if(!(formulaTermExpression == FormulaTermExpression.FORMULA)){
            throw new RuntimeException("QuantifierExpression can only be translated to formula.");
        }
        List<String> oldTypeParams = typeParams;
        if(typeParams == null){
            typeParams = new LinkedList<>();
        }
        typeParams.addAll(Arrays.asList(quantifierExpression.getTypeParams()));
        formulaTermExpression = FormulaTermExpression.FORMULA;
        quantifierExpression.getSubformula().accept(this);
        Formula subFormula = (Formula)resultNode;
        List<FormulaBinder> binders = new LinkedList<>();
        for(VarList varList: quantifierExpression.getParameters()){
            varList.accept(this);
            for(Pair<String,Type> binderEncoding: varListResult){
                //cannot have WhereClause.
                binders.add(new FormulaBinder(binderEncoding.getKey(),binderEncoding.getValue()));
            }
        }
        List<whyML.ast.formula.quantifier.Trigger> triggers = new LinkedList<>();
        for(Attribute attribute: quantifierExpression.getAttributes()){
            if(attribute instanceof Trigger){
                for(Expression e : ((Trigger)attribute).getTriggers()){
                    try{
                        formulaTermExpression = FormulaTermExpression.TERM;
                        e.accept(this);
                        triggers.add(new whyML.ast.formula.quantifier.Trigger((Term)resultNode));
                    }catch (RuntimeException ex){//try to create a formula if we cannot create a term.
                        formulaTermExpression = FormulaTermExpression.FORMULA;
                        e.accept(this);
                        triggers.add(new whyML.ast.formula.quantifier.Trigger((Formula)resultNode));
                    }
                }
            }else{
                //attributes which are not triggers could perhaps be processed based on predefined rules.
                Log.error("Did not process Attribute: "+attribute);
            }
        }
        resultNode = new QuantifiedFormula(quantifierExpression.isUniversal(),binders,triggers,subFormula);
        typeParams = oldTypeParams;
    }

    /**
     * translate unary expressions (e.g. -5, not a)
     * @param unaryExpression
     */
    @Override
    public void visit(UnaryExpression unaryExpression) {
        if(formulaTermExpression == FormulaTermExpression.FORMULA){
            Formula formula;
            if(UnaryOperator.LOGICNEG.equals(unaryExpression.getOperator())){
                unaryExpression.getExpr().accept(this);
                formula  = (Formula)resultNode;
                formula = new NegationFormula(formula);
            }else if(UnaryOperator.OLD.equals(unaryExpression.getOperator())) {
                formulaTermExpression = FormulaTermExpression.TERM;
                unaryExpression.getExpr().accept(this);
                Term term = (Term)resultNode;
                formula = new PredicateApplicationFormula("old", term);
            }else {
                throw new RuntimeException("Cannot create a formula with: "+unaryExpression);
            }
            resultNode = formula;
        }else if(formulaTermExpression == FormulaTermExpression.TERM){
            unaryExpression.getExpr().accept(this);
            Term term = (Term)resultNode;
            if(UnaryOperator.LOGICNEG.equals(unaryExpression.getOperator())){
                //can use either. notb is imported from boolean theory in WhyML
                //term = new PrefixTerm("not",term);
                term = new FunctionApplicationTerm("notb", term);
            }else if(UnaryOperator.ARITHNEGATIVE.equals(unaryExpression.getOperator())){
                String neg = "-";
                if(BoogieType.realType.equals(unaryExpression.getExpr().getType())){
                    neg = neg+".";
                }
                term = new PrefixTerm(neg,term);
            }else{
                term = new OldTerm(term);
            }
            resultNode = term;
        }else{//EXPRESSION
            unaryExpression.getExpr().accept(this);
            whyML.ast.expression.Expression expression = (whyML.ast.expression.Expression)resultNode;
            if(UnaryOperator.LOGICNEG.equals(unaryExpression.getOperator())){
                expression = new FunctionApplicationExpression(new SymbolExpression("notb"),expression);
            }else if(UnaryOperator.ARITHNEGATIVE.equals(unaryExpression.getOperator())){
                String neg = "-";
                if(BoogieType.realType.equals(unaryExpression.getExpr().getType())){
                    neg = neg+".";
                }
                expression = new PrefixExpression(neg,expression);
            }else{
                throw new RuntimeException("Cannot create old expression:"+unaryExpression);
            }
            resultNode = expression;
        }
    }

    /**
     * this only works as wild card expression for if or while conditions. Call-forall statements are replaced by equivalent
     * assume statements without wildcard expressions beforehand.
     * @param wildcardExpression
     */
    @Override
    public void visit(WildcardExpression wildcardExpression) {
        String havocBool = "havoc_bool";
        String havocArgument = "()";
        if(formulaTermExpression == FormulaTermExpression.FORMULA){
            resultNode = new PredicateApplicationFormula(havocBool, new SymbolTerm(havocArgument));
        }else if(formulaTermExpression == FormulaTermExpression.TERM){
            resultNode = new FunctionApplicationTerm(havocBool, new SymbolTerm(havocArgument));
        }else if(formulaTermExpression == FormulaTermExpression.EXPRESSION){
            //() is probably some special expression, but this works.
            resultNode = new FunctionApplicationExpression(new SymbolExpression(havocBool), new SymbolExpression(havocArgument));
        }else{//shouldn't be possible
            throw new RuntimeException("implement");
        }
    }


    private Pattern returnsSpecPattern;

    /**
     * if returnsSpecPattern is null, a Postcondition is created. otherwise a ReturnsSpecification is created.
     * nullifies returnsSpecPattern after each call.
     *
     * postconditoins are of the form: ensures {a > 5 && result < 3}
     * returnspecs are of the form:     returns {res |-> a > 5 && res < 3}
     *
     * returnspecs allow us to give any name to the returned parameter, and also split it up using tuples, e.g.
     * returns {(res1,res2) -> res1 < 7 }
     *
     * @param ensuresSpecification
     */
    @Override
    public void visit(EnsuresSpecification ensuresSpecification) {
        if (returnsSpecPattern == null){
            formulaTermExpression = FormulaTermExpression.FORMULA;
            ensuresSpecification.getFormula().accept(this);
            Postcondition postcondition = new Postcondition((Formula)resultNode);
            resultNode = postcondition;
        }else{
            formulaTermExpression = FormulaTermExpression.FORMULA;
            ensuresSpecification.getFormula().accept(this);
            ReturnsSpecification returnsSpecification = new ReturnsSpecification(new FormulaCase(returnsSpecPattern,(Formula)resultNode));
            resultNode = returnsSpecification;
        }
        returnsSpecPattern = null;

    }

    /**
     * translation of loop invariants
     * @param loopInvariantSpecification
     */
    @Override
    public void visit(LoopInvariantSpecification loopInvariantSpecification) {
        formulaTermExpression = FormulaTermExpression.FORMULA;
        loopInvariantSpecification.getFormula().accept(this);
        resultNode = new Invariant((Formula)resultNode);
    }

    /**
     * returns null if the modifies clause is empty.
     * @param modifiesSpecification
     */
    @Override
    public void visit(ModifiesSpecification modifiesSpecification) {
        if(modifiesSpecification.getIdentifiers().length == 0){
            resultNode = null;
            return;
        }
        List<Term> terms = new LinkedList<>();
        //this works, but also works if we add v.contents instead of v. Both have the same behavior.
        for(String ident : modifiesSpecification.getIdentifiers()){
            terms.add(new SymbolTerm(whyMLIdentifier(ident)));
        }
        resultNode = new WritesSpec(terms);
    }

    /**
     * translate preconditions.
     * @param requiresSpecification
     */
    @Override
    public void visit(RequiresSpecification requiresSpecification) {
        formulaTermExpression = FormulaTermExpression.FORMULA;
        requiresSpecification.getFormula().accept(this);
        Precondition precondition = new Precondition((Formula)resultNode);
        resultNode = precondition;
    }

    /**
     * translate assert statements.
     * @param assertStatement
     */
    @Override
    public void visit(AssertStatement assertStatement) {
        formulaTermExpression = FormulaTermExpression.FORMULA;
        assertStatement.getFormula().accept(this);
        resultNode = new AssertionExpression(AssertionExpression.AssertionType.ASSERT,(Formula)resultNode);
    }


    /**
     * local variables from Boogie are translated to global variables in whyML.
     * Therefore their names must be changed (because boogie supports local shadowing)
     * this function returns the equivalent WhyML name.
     * @param boogieIdentifier
     * @return
     */
    public String whyMLIdentifier(String boogieIdentifier){
        if(localVariableIdentifiers.contains(boogieIdentifier)){
            return localVariableIdentifierPrefix()+boogieIdentifier;
        }else{
            return boogieIdentifier;
        }
    }

    /**
     * translate an assignment. can be either an assignment to a variable, or to an array index of the form a[1] := false;
     * @param assignmentStatement
     */
    @Override
    public void visit(AssignmentStatement assignmentStatement) {

        if(formulaTermExpression != FormulaTermExpression.EXPRESSION){
            throw new RuntimeException("cannot translate assignment to term or formula");
        }
        if(assignmentStatement.getLhs().length > 1){
            //TODO add support for parallel assignment
            throw new RuntimeException("Parallel assignment not supported yet.");
        }
        whyML.ast.expression.Expression resultExpr;
        String lqualid = "contents";
        if(assignmentStatement.getLhs()[0] instanceof VariableLHS){
            VariableLHS variableLHS = (VariableLHS) assignmentStatement.getLhs()[0];
            whyML.ast.expression.Expression firstExpression = new SymbolExpression(whyMLIdentifier(variableLHS.getIdentifier()));

            formulaTermExpression = FormulaTermExpression.EXPRESSION;
            assignmentStatement.getRhs()[0].accept(this);
            whyML.ast.expression.Expression secondExpression = (whyML.ast.expression.Expression) resultNode;
            resultExpr = new FieldAssignmentExpression(firstExpression,lqualid,secondExpression);
        }else{
            //let arr = (set arr (i) (j)) in assume {true}
            ArrayLHS arrayLHS = (ArrayLHS)assignmentStatement.getLhs()[0];
            whyML.ast.expression.Expression functionName = new SymbolExpression("set");
            formulaTermExpression = FormulaTermExpression.EXPRESSION;
            arrayLHS.accept(this);
            whyML.ast.expression.Expression setArgument1 = arrayLHSResult1;//arr
            whyML.ast.expression.Expression setARgument2 = arrayLHSResult2;//(i)
            formulaTermExpression = FormulaTermExpression.EXPRESSION;
            assignmentStatement.getRhs()[0].accept(this);
            whyML.ast.expression.Expression setArgument3 = (whyML.ast.expression.Expression)resultNode;//(j)
            List<whyML.ast.expression.Expression> setArguments = new LinkedList<>();
            setArguments.add(setArgument1);
            setArguments.add(setARgument2);
            setArguments.add(setArgument3);
            FunctionApplicationExpression fae = new FunctionApplicationExpression(functionName,setArguments);
            //lqualid is already part of firstExpression, therefore set null.
            resultExpr = new FieldAssignmentExpression(setArgument1,null,fae);
        }
        resultNode = resultExpr;
    }

    /**
     * translate assume statements
     * @param assumeStatement
     */
    @Override
    public void visit(AssumeStatement assumeStatement) {
        formulaTermExpression = FormulaTermExpression.FORMULA;
        assumeStatement.getFormula().accept(this);
        resultNode = new AssertionExpression(AssertionExpression.AssertionType.ASSUME, (Formula)resultNode);
    }

    /**
     * translate break statements.
     * @param breakStatement
     */
    @Override
    public void visit(BreakStatement breakStatement) {
        if(breakStatement.getLabel() != null){
            //breaking to label should be handled as a special case of goto.
            throw new RuntimeException("implement: breaking to label not supported yet.");
        }
        resultNode = new ExceptionRaisingExpression("Break ");
    }

    /**
     * translate standard call statements. call-forall statements are replaced before this method is called.
     * @param callStatement
     */
    @Override
    public void visit(CallStatement callStatement) {
        if(callStatement.isForall()){//call forall
            //this should never be called since we replace all call-forall statements in the boogie ast
            //before we call this translator.
            throw new RuntimeException("call for-all was mistakenly not replaced.");
        }else{//standard call statement (not call-forall)
            if(!(formulaTermExpression == FormulaTermExpression.EXPRESSION)){
                throw new RuntimeException("call statement call only be translated to expression");
            }
            //process rhs
            List<whyML.ast.expression.Expression> rhsArguments = new LinkedList<>();
            for(Expression e: callStatement.getArguments()){
                formulaTermExpression = FormulaTermExpression.EXPRESSION;
                e.accept(this);
                rhsArguments.add((whyML.ast.expression.Expression)resultNode);
            }
            whyML.ast.expression.Expression rhsFunctionName = new SymbolExpression(callStatement.getMethodName());
            FunctionApplicationExpression rhs = new FunctionApplicationExpression(rhsFunctionName,rhsArguments);
            if(callStatement.getLhs() == null || callStatement.getLhs().length == 0){
                //lhs has 0 idents
                resultNode = rhs;
            }else if(callStatement.getLhs().length == 1){
                whyML.ast.expression.Expression assignee = new SymbolExpression(whyMLIdentifier(callStatement.getLhs()[0]));
                resultNode = new FieldAssignmentExpression(assignee,"contents",rhs);
            }else { //lengt > 1
                throw new RuntimeException("implement parallel assignment");
            }
        }
    }

    @Override
    public void visit(GotoStatement gotoStatement) {
        throw new RuntimeException("implement");
    }

    /**
     * translation of havoc statements. generates corresponding assume statements from where clauses.
     * Boogie:
     *      havoc x where x > 2;
     * WhyML:
     *      x.contents <- (havoc (x.contents));
     *      assume {x > 2}
     * @param havocStatement
     */
    @Override
    public void visit(HavocStatement havocStatement) {
        if(formulaTermExpression == FormulaTermExpression.EXPRESSION){
            List<whyML.ast.expression.Expression> havocAssignments = new LinkedList<>();
            //the order is important! first call havoc on identifiers left to right,
            //then for identifiers left to right, add assume statements for where clauses (after ALL havocs functions)
            for(String ident: havocStatement.getIdentifiers()){
                IdentifierExpression identifierExpression = new IdentifierExpression(null,ident);
                formulaTermExpression = FormulaTermExpression.EXPRESSION;
                visit(identifierExpression);
                whyML.ast.expression.Expression identExpr = (whyML.ast.expression.Expression)resultNode;
                FunctionApplicationExpression fae = new FunctionApplicationExpression(new SymbolExpression("havoc"),identExpr);
                FieldAssignmentExpression assignmentExpression = new FieldAssignmentExpression(identExpr,null,fae);
                havocAssignments.add(assignmentExpression);
            }
            for(String ident : havocStatement.getIdentifiers()){
                Formula whereClause = getWhereClause(ident);
                if(whereClause != null){
                    havocAssignments.add(new AssertionExpression(AssertionExpression.AssertionType.ASSUME, whereClause));
                }
            }
            resultNode = convertExpressionListToSingleExpression(havocAssignments);
        }else{
            throw new RuntimeException("havoc statements can only be translated to expressions.");
        }
    }

    /**
     * returns the Formula from the where clause if ident has a where clause (globally or locally).
     * returns null otherwise.
     * @param ident
     * @return
     */
    private Formula getWhereClause(String ident){
        Formula localFormula = localWhereClauses.get(ident);
        Formula globalFormul = globalWhereClauses.get(ident);
        if(localFormula != null && globalFormul != null){
            //this shouldn't happen, since we rename shadowed variables beforehand.
            Log.error("\"variable is defined globally and locally, something went wrong...\"");
            //throw new RuntimeException("variable is defined globally and locally, something went wrong...");
        }
        if(localFormula != null){
            return localFormula;
        }else {//might be null, don't care, return it.
            return globalFormul;
        }
    }

    /**
     * translation of if-then-else statements within procedures.
     * @param ifStatement
     */
    @Override
    public void visit(IfStatement ifStatement) {
        formulaTermExpression = FormulaTermExpression.EXPRESSION;
        ifStatement.getCondition().accept(this);
        whyML.ast.expression.Expression condition = (whyML.ast.expression.Expression)resultNode;
        whyML.ast.expression.Expression thenExpression = translateArrayOfStatements(ifStatement.getThenPart());
        whyML.ast.expression.Expression elseExpression = translateArrayOfStatements(ifStatement.getElsePart());
        resultNode = new ConditionalExpression(condition,thenExpression,elseExpression);
    }

    /**
     * converts the array of boogie statements into a semantically equivalent WhyML Expression.
     * If the array is empty, the expression "assume {true}" is returned.
     * @param statements
     * @return
     */
    private whyML.ast.expression.Expression translateArrayOfStatements(Statement[] statements){
        List<whyML.ast.expression.Expression> resultList = new LinkedList<whyML.ast.expression.Expression>();
        for(Statement s: statements){
            formulaTermExpression = FormulaTermExpression.EXPRESSION;
            s.accept(this);
            resultList.add((whyML.ast.expression.Expression)resultNode);
        }
        whyML.ast.expression.Expression result = convertExpressionListToSingleExpression(resultList);
        if(result == null){
            result = new AssertionExpression(AssertionExpression.AssertionType.ASSUME, new TrueFormula());
        }
        return result;
    }

    /**
     *
     * @param expression
     * @param handler
     * @return the input expression surrounded with a "try .. with" exception handling. Null if either of the inputs are null.
     */
    private ExceptionCatchingExpression surroundExpressionWithExceptionCatching(whyML.ast.expression.Expression expression, Handler handler){
        if(expression == null || handler == null){
            return null;
        }
        return new ExceptionCatchingExpression(expression,handler);
    }


    @Override
    public void visit(Label label) {
        //throw new RuntimeException("implement");
        Log.error("label not translated: "+label);
    }

    @Override
    public void visit(ParallelCall parallelCall) {
        throw new RuntimeException("implement ParallelCAll");
    }

    @Override
    public void visit(ReturnStatement returnStatement) {
        resultNode = new ExceptionRaisingExpression("Return ");
    }

    /**
     * can either return a WhileLoopExrpession, OR a sequence expression (this is the case if it contains free invariants, which are added as assume statements.)
     * @param whileStatement
     */
    @Override
    public void visit(WhileStatement whileStatement) {
        List<Invariant> invariants = new LinkedList<>();
        //formulas from free invariants as assume-statements
        List<AssertionExpression> freeInvariantAssumes = new LinkedList<>();
        for(LoopInvariantSpecification specification: whileStatement.getInvariants()){
            specification.accept(this);
            Invariant invariant = (Invariant)resultNode;
            invariants.add(invariant);
            if(specification.isFree()){
                freeInvariantAssumes.add(new AssertionExpression(AssertionExpression.AssertionType.ASSUME, invariant.getFormula()));
            }
        }
        formulaTermExpression = FormulaTermExpression.EXPRESSION;
        whileStatement.getCondition().accept(this);
        whyML.ast.expression.Expression conditionExpression = (whyML.ast.expression.Expression) resultNode;

        List<whyML.ast.expression.Expression> expressionList = new LinkedList<>();
        //add assume statements from free invariants to beginning of loop body.
        expressionList.addAll(freeInvariantAssumes);
        for(Statement s: whileStatement.getBody()){
            formulaTermExpression = FormulaTermExpression.EXPRESSION;
            s.accept(this);
            expressionList.add((whyML.ast.expression.Expression)resultNode);
        }
        //add assume statements from free invariants to end of loop body.
        expressionList.addAll(freeInvariantAssumes);
        whyML.ast.expression.Expression loopBodyExpression = convertExpressionListToSingleExpression(expressionList);
        whyML.ast.expression.Expression loopExpression = new WhileLoopExpression(conditionExpression,invariants,loopBodyExpression);
        //if we have free invariants, we must add them as assume statements before the loop.
        List<whyML.ast.expression.Expression> resultList = new LinkedList<>();
        resultList.addAll(freeInvariantAssumes);
        resultList.add(loopExpression);
        resultNode = convertExpressionListToSingleExpression(resultList);
    }

    @Override
    public void visit(YieldStatement yieldStatement) {
        throw new RuntimeException("implement YieldStatement");
    }

    private whyML.ast.expression.Expression arrayLHSResult1;
    private whyML.ast.expression.Expression arrayLHSResult2;

    /**
     * IMPORTANT: doesn't set result in resultNode, but in arrayLHSResult1 and 2.
     * translation of an array LHS, which is a special case of an left hand side of an assignment.
     * @param arrayLHS
     */
    @Override
    public void visit(ArrayLHS arrayLHS) {
        if(formulaTermExpression != FormulaTermExpression.EXPRESSION){
            throw new RuntimeException("cannot translate ArrayLHS to formula or term");
        }
        arrayLHS.getArray().accept(this);
        if(!(arrayLHS.getArray() instanceof  ArrayLHS)){
            arrayLHSResult1 = (whyML.ast.expression.Expression)resultNode;
        }
        if(arrayLHS.getIndices().length == 1){
            formulaTermExpression = FormulaTermExpression.EXPRESSION;
            arrayLHS.getIndices()[0].accept(this);
            arrayLHSResult2 = (whyML.ast.expression.Expression)resultNode;
        }else if(arrayLHS.getIndices().length == 0){
            throw new RuntimeException("Cannot handle arrays which do not take any arguments yet");
        }else{
            List<whyML.ast.expression.Expression> expressionList = new LinkedList<>();
            for(Expression e: arrayLHS.getIndices()){
                formulaTermExpression = FormulaTermExpression.EXPRESSION;
                e.accept(this);
                expressionList.add((whyML.ast.expression.Expression)resultNode);
            }
            arrayLHSResult2 = new TupleExpression(expressionList);
        }
        resultNode = null;
    }

    /**
     * not needed, since we translate bodies inplace.
     * @param body
     */
    @Override
    public void visit(Body body) {
        Log.error("body shouldn't be visited.");
    }

    /**
     * not needed for the moment. Translation of special attributes could be added in the future.
     * @param namedAttribute
     */
    @Override
    public void visit(NamedAttribute namedAttribute) {
        Log.error("NamedAttribute shouldn't be visited.");
    }

    /**
     * not needed. parent edges are axiomatized when constants are visited.
     * @param parentEdge
     */
    @Override
    public void visit(ParentEdge parentEdge) {
        Log.error("ParentEdge shouldn't be visited.");
    }

    /**
     * not needed. we go through declarations directly at the beginning, don't need access to the project.
     * @param project
     */
    @Override
    public void visit(Project project) {
        Log.error("Project shouldn't be visited.");
    }

    /**
     * not needed, as triggers are translated inplace.
     * @param trigger
     */
    @Override
    public void visit(Trigger trigger) {
        Log.error("Trigger shouldn't be visited.");
    }

    /**
     * not needed. we go through declarations directly at the beginning, don't need access to the unit.
     * @param unit
     */
    @Override
    public void visit(Unit unit) {
        Log.error("Unit shouldn't be visited.");
    }

    //private String lastVariableLHSName;

    @Override
    public void visit(VariableLHS variableLHS) {
        if(formulaTermExpression == FormulaTermExpression.FORMULA){
            throw new RuntimeException("VarialbeLHS cannot be Formula");
        }else if(formulaTermExpression == FormulaTermExpression.TERM){
            throw new RuntimeException("VarialbeLHS cannot be Term");
        }else {//expression
            if(isVariable(variableLHS.getIdentifier())){
                resultNode = new SymbolExpression(whyMLIdentifier(variableLHS.getIdentifier())+".contents");
            }else{
                //if lhs is a map, this is called eventually.
                resultNode = new SymbolExpression(whyMLIdentifier(variableLHS.getIdentifier()));
            }
        }
    }

    /**
     * returns true if localVariableIdentifiers or globalVariableIdentifiers contains s
     * @param s
     * @return
     */
    private boolean isVariable(String s){
        return (localVariableIdentifiers.contains(s) || globalVariableIdentifiers.contains(s));
    }


    private List<Pair<String,Type>> varListResult;
    //is set by varlist. is set to true, if there is no where clause.
    private Formula whereClauseFormulaResult;
    //true if the varlist should be added to local variables.
    private boolean addToLocalVariables = false;

    /**
     * checks addToLocalVariables. if it is true, all idents are added to lcoal variables. Also sets addToLocalVariables to false again.
     * stores the vars in varListResult.
     * stores formula for whereClause in whereClauseFormulaResult. If there is no whereClause, it is set to TrueFormula.
     * returns null as variable name, if there is no name, only a type.
     * @param varList
     */
    @Override
    public void visit(VarList varList) {
        varListResult = new LinkedList<Pair<String, Type>>();
        varList.getType().accept(this);
        Type varType = resultType;
        for(String s: varList.getIdentifiers()){
            varListResult.add(new Pair<String, Type>(s,varType));
            if(addToLocalVariables){
                localVariableIdentifiers.add(s);
            }
        }
        if(varListResult.isEmpty()){
            varListResult.add(new Pair<String, Type>(null,varType));
        }

        if(varList.getWhereClause() == null){
            whereClauseFormulaResult = new TrueFormula();
        }else{
            formulaTermExpression = FormulaTermExpression.FORMULA;
            varList.getWhereClause().accept(this);
            whereClauseFormulaResult = (Formula) resultNode;
        }
        addToLocalVariables = false;
    }

    /**
     *
     * @return name of the next axiom. axioms are named A0, A1, A2 ....
     */
    private String generateNextAxiomIdent(){
        return "A"+(axiomCounter++);
    }

    /**
     * translate an axiom.
     * @param axiom
     */
    @Override
    public void visit(Axiom axiom) {
        formulaTermExpression = FormulaTermExpression.FORMULA;
        axiom.getFormula().accept(this);
        whyML.ast.declerations.Axiom whyAxiom = new whyML.ast.declerations.Axiom(generateNextAxiomIdent(),(Formula)resultNode);
        whyMLDeclerations.add(whyAxiom);
    }

    /**
     * translate constant declarations. also generate needed axioms for uniqueness etc. some axioms are generated inplace
     * whereas others are generated using the class ConstantAxiomGenerator.
     * @param constDeclaration
     */
    @Override
    public void visit(ConstDeclaration constDeclaration) {
        //1. create constant delcarations
        //2. create needed axioms.
        //1.
        typeParams = new LinkedList<>();
        VarList varList = constDeclaration.getVarList();
        varList.accept(this);
        for(Pair<String,Type> identTypePair: varListResult){
            ConstantDeclaration cd = new ConstantDeclaration(identTypePair.getKey(),identTypePair.getValue());
            whyMLDeclerations.add(cd);
            //2.
            //most axioms are generated in Class ConstantAxiomGenerator.
            //empty parent edge is easier to generate here, so that's what we do.
            //parent edge is empty, but NOT null.
            if(constDeclaration.getParentInfo() != null){
                if((constDeclaration.getParentInfo().length == 0)){
                    whyMLDeclerations.add(constantAxiomGenerator.generateEmptyParentEdgeAxiom(identTypePair.getKey(),identTypePair.getValue()));
                }else{//one or more parents.
                    List<String> parents = Arrays.asList(constDeclaration.getParentInfo()).stream().map(parentEdge -> parentEdge.getIdentifier()).collect(Collectors.toList());
                    whyMLDeclerations.add(constantAxiomGenerator.generateFullParentEdgeAxiom(identTypePair.getKey(), parents, identTypePair.getValue()));
                }
            }
        }
        typeParams = null;
    }


    /**
     * Function Declarations must be axiomatized in WhyML. this function creates the formula for the axiom.
     * @param functionApplicationTerm
     * @param boogieExpression
     * @return
     */
    private Formula generateFunctionaxiomFormula(FunctionApplicationTerm functionApplicationTerm, Expression boogieExpression){
        if(!(boogieExpression instanceof IfThenElseExpression)){
            if(BoogieType.boolType.equals(boogieExpression.getType())){//changed from quantifier expression to all bool types.
                //QuantifierExpression qe = (QuantifierExpression)boogieExpression;
                formulaTermExpression = FormulaTermExpression.FORMULA;
                boogieExpression.accept(this);
                Formula f = (Formula)resultNode;
                //if the body of a function declaration is a quantified expression, then we know the return type is bool, so its a predicate.
                PredicateApplicationFormula paf = new PredicateApplicationFormula(functionApplicationTerm.getLqualid(),functionApplicationTerm.getTerms());
                return new BinaryFormula(paf,"<->",f);
            }else{
                formulaTermExpression = FormulaTermExpression.TERM;
                boogieExpression.accept(this);
                Term term2 = (Term) resultNode;
                return new InfixFormula(functionApplicationTerm,"=",term2);
            }
        }else{
            IfThenElseExpression ifThenElseExpression = (IfThenElseExpression) boogieExpression;
            formulaTermExpression = FormulaTermExpression.FORMULA;
            ifThenElseExpression.getCondition().accept(this);
            Formula conditionFormula = (Formula) resultNode;
            Formula leftConjunctionFormula = new BinaryFormula(conditionFormula,"->",generateFunctionaxiomFormula(functionApplicationTerm,ifThenElseExpression.getThenPart()));
            Formula rightConjunctionFormula = new BinaryFormula(new NegationFormula(conditionFormula),"->",generateFunctionaxiomFormula(functionApplicationTerm,ifThenElseExpression.getElsePart()));
            return new BinaryFormula(leftConjunctionFormula,"/\\",rightConjunctionFormula);
        }
    }

    /**
     * function declarations from boogie are translated into functions, and axiomatized if they have a body.
     * @param functionDeclaration
     */
    @Override
    public void visit(FunctionDeclaration functionDeclaration) {
        //1. create function declaration without body
        //2. axiomatize body

        //1.
        typeParams = new LinkedList<>();
        typeParams.addAll(Arrays.asList(functionDeclaration.getTypeParams()));
        //typeParams = Arrays.asList(functionDeclaration.getTypeParams());
        String ident = functionDeclaration.getIdentifier();
        functionDeclaration.getOutParam().accept(this);
        //out param name is optional.
        Type outParamType = varListResult.get(0).getValue();
        List<Param> inputParams = new LinkedList<Param>();
        List<Term> functionApplicationArguments = new LinkedList<Term>();//this is needed for part 2
        List<FormulaBinder> axiomBinders = new LinkedList<FormulaBinder>();//this is needed for part 2
        for(VarList varList: functionDeclaration.getInParams()){
            varList.accept(this);
            for(Pair<String,Type> paramEncoding: varListResult){
                inputParams.add(new Param(paramEncoding.getKey(),paramEncoding.getValue()));
                functionApplicationArguments.add(new SymbolTerm(paramEncoding.getKey()));
                axiomBinders.add(new FormulaBinder(paramEncoding.getKey(),paramEncoding.getValue()));
            }
        }
        whyML.ast.declerations.FunctionDeclaration whyMLFunctionDecl = new whyML.ast.declerations.FunctionDeclaration(ident,inputParams,outParamType,false);
        whyMLDeclerations.add(whyMLFunctionDecl);

        //2.
        if(functionDeclaration.getBody() != null){
            FunctionApplicationTerm functionApplicationTerm = new FunctionApplicationTerm(ident,functionApplicationArguments);
            Formula axiomFormula1 = generateFunctionaxiomFormula(functionApplicationTerm,functionDeclaration.getBody());
            QuantifiedFormula quantifiedAxiomFormula = new QuantifiedFormula(true,axiomBinders,axiomFormula1);
            whyML.ast.declerations.Axiom axiom = new whyML.ast.declerations.Axiom(generateNextAxiomIdent(),quantifiedAxiomFormula);
            whyMLDeclerations.add(axiom);
        }
        typeParams = null;
    }

    /**
     * We don't need to do anything for implementations.
     * Each Implementation body is processed in visit ProcedureDeclaration, using getBodiesOfProcedure.
     * @param implementation
     */
    @Override
    public void visit(Implementation implementation) {
        //do nothing.
    }

    Map<String,Formula> localWhereClauses;

    @Override
    public void visit(ProcedureDeclaration procedureDeclaration) {
        //1. translate to abstract function (no body, only specification)
        //2. translate to 'let' procedure

        //stores WhyML name of variable as key.
        localWhereClauses = new HashMap<String,Formula>();
        localVariableIdentifiers = new HashSet<String>();
        currentProcedureName = procedureDeclaration.getIdentifier();
        //1.
        String lIdent = procedureDeclaration.getIdentifier();
        List<Param> inputArguments = new LinkedList<Param>();
        typeParams = new LinkedList<>();
        typeParams.addAll(Arrays.asList(procedureDeclaration.getTypeParams()));
        for(VarList varList: procedureDeclaration.getInParams()){
            addToLocalVariables = false;
            varList.accept(this);
            for(Pair<String,Type> inputParameter: varListResult){
                inputArguments.add(new Param(localVariableIdentifierPrefix()+inputParameter.getKey(),inputParameter.getValue()));
                if(!(new TrueFormula()).equals(whereClauseFormulaResult)) {
                    localWhereClauses.put(inputParameter.getKey(), whereClauseFormulaResult);
                }
            }
        }

        List<Param> outputArguments = new LinkedList<Param>();

        for(VarList varList: procedureDeclaration.getOutParams()){
            addToLocalVariables = false;
            varList.accept(this);
            for(Pair<String,Type> outputParameter: varListResult){
                outputArguments.add(new Param(outputParameter.getKey(),outputParameter.getValue()));
                if(!(new TrueFormula()).equals(whereClauseFormulaResult)) {
                    //adding to wherclause must be done AFTER spec generations (so names are ok) and with addToLocalVariables set to true!
                    //same with inParams
                    //localWhereClauses.put(outputParameter.getKey(), whereClauseFormulaResult);
                }
            }
        }
        //return type
        Type returnType;
        if(outputArguments.size() == 1){
            returnType = outputArguments.get(0).getType();
        }else if(outputArguments.size() > 1){
            List<Type> outputTypes = new LinkedList<Type>();
            for(Param p: outputArguments){
                outputTypes.add(p.getType());
            }
            returnType = new TypeTuple(outputTypes);
        }else{
            returnType = new EmptyTupleType();
        }

        List<Spec> specsAbstract = new LinkedList<Spec>();
        List<Spec> specsImplementation = new LinkedList<>();
        List<Specification> abstractSpecifications = contractGenerator.getAbstractProcedureContracts(procedureDeclaration.getIdentifier());

        for(Specification specification: abstractSpecifications){
            if(specification instanceof EnsuresSpecification){
                EnsuresSpecification ensuresSpecification = (EnsuresSpecification) specification;
                if(outputArguments.size() == 1){//output is single type
                    returnsSpecPattern = new VariablePattern(outputArguments.get(0).getName());
                    ensuresSpecification.accept(this);

                }else if(outputArguments.size() > 1){//output is tuple type -> need to create tuple pattern
                    List<Pattern> patterns = new LinkedList<Pattern>();
                    for(Param outputParam: outputArguments){
                        patterns.add(new VariablePattern(outputParam.getName()));
                    }
                    returnsSpecPattern = new TuplePattern(patterns);
                    ensuresSpecification.accept(this);
                }else{
                    //throw new RuntimeException("Procedure Declaration has no return parameters at: "+procedureDeclaration.getLocation().toString());
                    returnsSpecPattern = null;
                    ensuresSpecification.accept(this);
                }
            }else{
                specification.accept(this);
            }
            if(resultNode != null){
                specsAbstract.add((Spec) resultNode);
            }
        }

        //add whereClauses as return spec clause or postconditions to val abstract function.
        for(VarList varList : procedureDeclaration.getOutParams()){
            varList.accept(this);
            if(!(new TrueFormula()).equals(whereClauseFormulaResult)){
                Pattern specPattern;
                if(outputArguments.size() == 1){//output is single type
                    specPattern = new VariablePattern(outputArguments.get(0).getName());
                    specsAbstract.add(new ReturnsSpecification(new FormulaCase(specPattern,whereClauseFormulaResult)));
                }else if(outputArguments.size() > 1){//output is tuple type -> need to create tuple pattern
                    List<Pattern> patterns = new LinkedList<Pattern>();
                    for(Param outputParam: outputArguments){
                        patterns.add(new VariablePattern(outputParam.getName()));
                    }
                    specPattern = new TuplePattern(patterns);
                    specsAbstract.add(new ReturnsSpecification(new FormulaCase(specPattern,whereClauseFormulaResult)));
                }else{
                    //if there are no output arguments, there also can't be any output WhereClauses
                }
            }
        }

        List<Specification> implementationSpecifications = contractGenerator.getProcedureImplementationContracts(procedureDeclaration.getIdentifier());
        for(Specification specification: implementationSpecifications){
            if(specification instanceof EnsuresSpecification){
                EnsuresSpecification ensuresSpecification = (EnsuresSpecification) specification;
                if(outputArguments.size() == 1){//output is single type
                    returnsSpecPattern = new VariablePattern(outputArguments.get(0).getName());
                    ensuresSpecification.accept(this);
                }else if(outputArguments.size() > 1){//output is tuple type -> need to create tuple pattern
                    List<Pattern> patterns = new LinkedList<Pattern>();
                    for(Param outputParam: outputArguments){
                        patterns.add(new VariablePattern(outputParam.getName()));
                    }
                    returnsSpecPattern = new TuplePattern(patterns);
                    ensuresSpecification.accept(this);
                }else{
                    //throw new RuntimeException("Procedure Declaration has no return parameters at: "+procedureDeclaration.getLocation().toString());
                    returnsSpecPattern = null;
                    ensuresSpecification.accept(this);
                }
            }else{
                specification.accept(this);
            }
            if(resultNode != null){
                specsImplementation.add((Spec)resultNode);
            }
        }

        for(VarList varList: procedureDeclaration.getOutParams()){
            addToLocalVariables = true;
            varList.accept(this);
            for(Pair<String,Type> outputParameter: varListResult){
                if(!(new TrueFormula()).equals(whereClauseFormulaResult)) {
                    localWhereClauses.put(outputParameter.getKey(), whereClauseFormulaResult);
                }
            }
        }

        ValAbstractFunction abstractFunction = new ValAbstractFunction(lIdent,inputArguments,returnType,specsAbstract);
        whyMLDeclerations.add(abstractFunction);

        //2.
        //If the procedure has no body, we are done.
        if(getBodiesOfProcedure(procedureDeclaration.getIdentifier()).isEmpty()){
            return;
        }
        procedureTypeParams = Arrays.asList(procedureDeclaration.getTypeParams());
        int implementationCounter = 0;
        //TODO if (any) body is empty, we could generate a lemma of the form preconditions -> postconditions.
        //TODO This could act as a call-forall replacement. However, it must be optional to not create clutter.
        //process all bodies from getBodiesOfProcedure (including those of different implementations)
        for(Body body: getBodiesOfProcedure(procedureDeclaration.getIdentifier())){
            VariableDeclaration[] variableDeclarations = body.getLocalVars();
            //generate local bindings for local variable defintions.
            localVariableBindingResult = new LinkedList<Pair<Pattern, whyML.ast.expression.Expression>>();
            for(VariableDeclaration v: variableDeclarations){
                isGlobalVariableDeclaration = false;
                formulaTermExpression = FormulaTermExpression.EXPRESSION;
                v.accept(this);
            }
            //add output parameters as local bindings
            for(Param p: outputArguments){
                localVariableIdentifiers.add(p.getName());
                //need to create abstract function so we don't get error if variable is a type variable: error: "type variable 'a cannot be generalized"
                //only create it once, in case two implementations or procedures have the same output argument
                if(!globalLocalVals.contains(globalLocalVariableIdentifierPrefix()+p.getName())){
                    globalLocalVals.add(globalLocalVariableIdentifierPrefix()+p.getName());
                    whyMLDeclerations.add(new ValAbstractFunction(globalLocalVariableIdentifierPrefix()+p.getName(),p.getType()));
                }
                whyML.ast.expression.Expression f1 = new SymbolExpression("havoc");
                FunctionApplicationExpression functionApplicationExpression = new FunctionApplicationExpression(new SymbolExpression(globalLocalVariableIdentifierPrefix()+p.getName()),new SymbolExpression("()"));
                whyML.ast.expression.Expression arg = new FunctionApplicationExpression(f1, functionApplicationExpression);
                whyML.ast.expression.Expression initExpression = new FunctionApplicationExpression(new SymbolExpression("ref"), arg);
                localVariableBindingResult.add(new Pair<>(new VariablePattern(localVariableIdentifierPrefix()+p.getName()), initExpression));
            }
            //generate preamble expression. first local bindings for local variable declarations, then assume statements for
            whyML.ast.expression.Expression preambleExpression;
            //the assume clauses of the preamble. either as assumeExpression, or as sequential expression.
            whyML.ast.expression.Expression preambleAssumes = null;
            //generate assume statements for preamble for each where clause.
            if(globalWhereClauses.entrySet().size() >= 1) {
                List<whyML.ast.expression.Expression> whereClauseList = new LinkedList<whyML.ast.expression.Expression>();
                Iterator it = globalWhereClauses.entrySet().iterator();
                while(it.hasNext()){
                    Map.Entry<String, Formula> pair = (Map.Entry) it.next();
                    whereClauseList.add(new AssertionExpression(AssertionExpression.AssertionType.ASSUME, pair.getValue()));
                    preambleAssumes = convertExpressionListToSingleExpression(whereClauseList);
                }
            }else {
                preambleAssumes = new AssertionExpression(AssertionExpression.AssertionType.ASSUME, new TrueFormula());
            }
            //add local where clauses (without overwriting global ones of course)
            if(localWhereClauses.entrySet().size() >= 1) {
                List<whyML.ast.expression.Expression> whereClauseList = new LinkedList<whyML.ast.expression.Expression>();
                whereClauseList.add(preambleAssumes);
                Iterator it = localWhereClauses.entrySet().iterator();
                while(it.hasNext()){
                    Map.Entry<String, Formula> pair = (Map.Entry) it.next();
                    whereClauseList.add(new AssertionExpression(AssertionExpression.AssertionType.ASSUME, pair.getValue()));
                    preambleAssumes = convertExpressionListToSingleExpression(whereClauseList);
                }
            }else {
                //no need to add another assume statement, enough to have 1 statement at least.
                //preambleAssumes = new AssertionExpression(AssertionExpression.AssertionType.ASSUME, new TrueFormula());
            }

            //merge local bindings and assume statements to one single expression.
            preambleExpression = generateLocalBindings(localVariableBindingResult,preambleAssumes);
            //generate body expression, surround it with try catch for return statemetn, add preamble to it.
            whyML.ast.expression.Expression letDeclarationExpression = ProcedureDeclarationTranslator.generateLetDeclarationExpression(body,preambleExpression,outputArguments,this);
            //we don't need recursive let declarations, because a call statement calls the val (and only the specs are checked).
            //specs are slightly different than val declaration. (because of free pre and post conditions)
            LetDeclaration letDeclaration = new LetDeclaration(lIdent+"_IMPL"+(implementationCounter++),inputArguments,returnType,specsImplementation,letDeclarationExpression);
            whyMLDeclerations.add(letDeclaration);
        }
        //clean up
        localVariableBindingResult = null;
        localVariableIdentifiers = new HashSet<String>();
        localWhereClauses = null;
        typeParams = null;
        procedureTypeParams = new LinkedList<>();
        currentProcedureName = null;
    }

    /**
     * used so global variables for local initialization are not added twice.
     */
    Set<String> globalLocalVals = new HashSet<>();

    /**
     * puts all the local bindings after eachother, and nextExpression as the nextExpression of the last local binding.
     * @param variableBindings
     * @param nextExpression
     * @return null if variableBindings is empty and nextExpression is null. Otherwise an expression which contains all local bindings, in reverse order of the list.
     */
    private whyML.ast.expression.Expression generateLocalBindings(List<Pair<Pattern, whyML.ast.expression.Expression>> variableBindings, whyML.ast.expression.Expression nextExpression){
        if(variableBindings.isEmpty()){
            return nextExpression;
        }
        //this reverses the list order, but doesn't matter.
        LocalBindingExpression result = new LocalBindingExpression(variableBindings.get(0).getKey(),variableBindings.get(0).getValue(),nextExpression);
        for(int i = 1; i < variableBindings.size(); ++i){
            Pattern currentPattern = variableBindings.get(i).getKey();
            whyML.ast.expression.Expression currentExpress = variableBindings.get(i).getValue();
            result = new LocalBindingExpression(currentPattern,currentExpress,result);
        }
        return result;
    }


    /**
     *  convert a list of expressions to a single expression. (i.e. it is turned into a tree hierarchy in the AST tree).
     * @param expressionList
     * @return null if list is null or empty. first element if list has length 1. A SequenceExpression if list has >= 2 elements.
     */
    private whyML.ast.expression.Expression convertExpressionListToSingleExpression(List<whyML.ast.expression.Expression> expressionList){
        return ProcedureDeclarationTranslator.convertExpressionListToSingleExpression(expressionList);
    }

    /**
     * map from a name of a type, to a list of type variables which are in the type synonym.
     * e.g.
     * type t1 v1 v2 = <w1,w2>[[[v1]w1]v2]w2;
     * synonymTypeVariableMap.get("t1") == [w1,w2]
     * should never be null (can be array of length 0)
     */
    private Map<String, String[]> synonymTypeVariableMap = new HashMap<>();

    /**
     * translate type declarations. Does not work correctly with types which contain type variables for a map, such as <a>[int]a
     * @param typeDeclaration
     */
    @Override
    public void visit(TypeDeclaration typeDeclaration) {
        int synonymParamLength = 0;
        if(typeDeclaration.getSynonym() instanceof ArrayAstType){
            synonymParamLength = ((ArrayAstType)typeDeclaration.getSynonym()).getTypeParams().length;
        }

        TypeVariable[] typeVariables = new TypeVariable[typeDeclaration.getTypeParams().length + synonymParamLength];
        for(int i = 0; i < typeDeclaration.getTypeParams().length; ++i){
            typeVariables[i] = new TypeVariable(typeDeclaration.getTypeParams()[i]);
        }
        if(typeDeclaration.getSynonym() instanceof ArrayAstType){
            ArrayAstType arrayAstType = (ArrayAstType) typeDeclaration.getSynonym();
            for(int i = 0; i < arrayAstType.getTypeParams().length;++i){
                typeVariables[typeDeclaration.getTypeParams().length + i] = new TypeVariable(arrayAstType.getTypeParams()[i]);
            }
        }

        if(typeDeclaration.getSynonym() == null){
           synonymTypeVariableMap.put(typeDeclaration.getIdentifier(),new String[0]);
        }else{
            if(typeDeclaration.getSynonym() instanceof ArrayAstType){
                ArrayAstType synonym = (ArrayAstType)typeDeclaration.getSynonym();
                synonymTypeVariableMap.put(typeDeclaration.getIdentifier(),synonym.getTypeParams());
            }else{
                synonymTypeVariableMap.put(typeDeclaration.getIdentifier(),new String[0]);
            }

        }

        whyML.ast.declerations.TypeDeclaration td;
        //the abstract function to create the havoc function for new types.
        //Code after here is probably not needed anymore and can be removed.
        List<Param> inputArguments = new LinkedList<Param>();
        int argumentCounter = 0;
        for(TypeVariable tv : typeVariables){
            Param param = new Param("x"+(argumentCounter++), tv);
            inputArguments.add(param);
        }
        //this ValAbstractFunction is not needed anymore, can be removed.
        ValAbstractFunction abstractFunction = new ValAbstractFunction("havoc_"+typeDeclaration.getIdentifier(),inputArguments,new TypeSymbol(typeDeclaration.getIdentifier(), Arrays.asList(typeVariables)));//
        if(typeDeclaration.getSynonym() == null){
            td = new whyML.ast.declerations.TypeDeclaration(typeDeclaration.getIdentifier(),typeVariables,null);
        }else{//we have a type synonym.
            //Boogie synonym Type
            ASTType typeSynonym = typeDeclaration.getSynonym();

            typeParams = new LinkedList<>();
            typeParams.addAll( Arrays.asList(typeDeclaration.getTypeParams()));
            if(typeSynonym instanceof ArrayAstType){
                for(String s : ((ArrayAstType)typeSynonym).getTypeParams()){
                    typeParams.add(s);
                }
            }
            typeSynonym.accept(this);
            //WhyML alias Type
            Type aliasType = resultType;
            td = new whyML.ast.declerations.TypeDeclaration(typeDeclaration.getIdentifier(),typeVariables,aliasType);
        }
        whyMLDeclerations.add(td);
        whyMLDeclerations.add(abstractFunction);
        typeParams = null;
    }



    List<Pair<Pattern, whyML.ast.expression.Expression>> localVariableBindingResult;

    /**
     * Translation of Variable declarations, which can either be global or local. Since we must handle these differently
     * in WhyML, the translation behaves differently depending on which case we are in:
     *
     * local variable stores result in localVariableBindingResult.
     * global variable declaration adds result to whyMLDeclerations and updates globalWhereClauses if necessary.
     * @param variableDeclaration
     */
    @Override
    public void visit(VariableDeclaration variableDeclaration) {
        //need to differentiate between global variable declaration, and local variable binding(in Boogie they are
        //both VariableDeclaration, in WhyML they are different).
        typeParams = new LinkedList<String>();
        if(isGlobalVariableDeclaration){
            for(VarList varList: variableDeclaration.getVariables()){
                varList.accept(this);
                for(Pair<String,Type> varNameTypePair: varListResult ){
                    Type t = varNameTypePair.getValue();
                    t.addRef();
                    Pair<String,Type> varWithRefType = new Pair<String, Type>(varNameTypePair.getKey(),t);
                    ValGlobalVariable globalVariable = new ValGlobalVariable(false,varWithRefType.getKey(),new whyML.ast.Label(false),varWithRefType.getValue());
                    whyMLDeclerations.add(globalVariable);
                    if (varList.getWhereClause() != null){
                        formulaTermExpression = FormulaTermExpression.FORMULA;
                        varList.getWhereClause().accept(this);
                        globalWhereClauses.put(varWithRefType.getKey(),(Formula)resultNode);
                    }
                }
            }

        }else{
            for(VarList varList: variableDeclaration.getVariables()){
                varList.accept(this);
                for(Pair<String,Type> varNameTypePair: varListResult ){
                    Type t = varNameTypePair.getValue();
                    //instead of local binding, use global variable declaration.
                    //whyMLDeclerations.add(new ValGlobalVariable(false, globalLocalVariableIdentifierPrefix()+varNameTypePair.getKey(),new whyML.ast.Label(false),t));
                    whyMLDeclerations.add(new ValAbstractFunction(globalLocalVariableIdentifierPrefix()+varNameTypePair.getKey(),t));
                    localVariableIdentifiers.add(varNameTypePair.getKey());
                    //havoc is not really necessary anymore, because the global declarations are changed to abstract functions, but it doesn't hurt.
                    whyML.ast.expression.Expression f1 = new SymbolExpression("havoc");
                    whyML.ast.expression.Expression arg = new FunctionApplicationExpression(f1, new FunctionApplicationExpression(new SymbolExpression(globalLocalVariableIdentifierPrefix()+varNameTypePair.getKey()),new SymbolExpression("()")));
                    whyML.ast.expression.Expression initExpression = new FunctionApplicationExpression(new SymbolExpression("ref"), arg);

                    Pattern localVariablePattern = new VariablePattern(localVariableIdentifierPrefix()+varNameTypePair.getKey());
                    localVariableBindingResult.add(new Pair<>(localVariablePattern,initExpression));

                    //this is done because when visiting varlist, this is generated, but the identifier is not in the local variable bindings yet, so it is not created correctly.
                    if(varList.getWhereClause() == null){
                        whereClauseFormulaResult = new TrueFormula();
                    }else{
                        formulaTermExpression = FormulaTermExpression.FORMULA;
                        varList.getWhereClause().accept(this);
                        whereClauseFormulaResult = (Formula) resultNode;
                    }
                    localWhereClauses.put(varNameTypePair.getKey(),whereClauseFormulaResult);
                }
            }
        }
    }

    /**
     * translateion of types which contain a map.
     * @param arrayAstType
     */
    @Override
    public void visit(ArrayAstType arrayAstType) {
        List<String> oldTypeParams = typeParams;
        typeParams.addAll(Arrays.asList(arrayAstType.getTypeParams()));

        TypeSymbol res = new TypeSymbol("map");
        if(arrayAstType.getIndexTypes().length >= 2){//tuple type.
            List<Type> allTypes = new LinkedList<Type>();
            for(ASTType astType: arrayAstType.getIndexTypes()){
                astType.accept(this);
                Type subType = resultType;
                allTypes.add(subType);
            }
            TypeTuple typeTuple = new TypeTuple(allTypes);
            res.addSubType(typeTuple);
        }else if (arrayAstType.getIndexTypes().length == 1){//single parameter
            arrayAstType.getIndexTypes()[0].accept(this);
            Type subType = resultType;
            res.addSubType(subType);
        }else{
            //translation of empty map, e.g. []int
            throw new RuntimeException("implement empty map");
        }
        arrayAstType.getValueType().accept(this);
        Type subType = resultType;
        res.addSubType(subType);
        typeParams = oldTypeParams;
        resultType = res;
    }


    private List<String> typeParams;
    //contains type params which are in the scope of a procedure.
    private List<String> procedureTypeParams;

    /**
     * IMPORTANT needs typeParams to be set with list of types which are params.
     * translation of a user defined type
     * @param namedAstType
     */
    @Override
    public void visit(NamedAstType namedAstType) {
        List<Type> subtypes = new LinkedList<Type>();
        for(ASTType at: namedAstType.getTypeArgs()){
            at.accept(this);
            subtypes.add(resultType);
        }
        //learning java 8 streams :) convert array of strings to array of type variables.
        if(synonymTypeVariableMap.get(namedAstType.getName())!= null){
            subtypes.addAll(Arrays.asList(synonymTypeVariableMap.get(namedAstType.getName())).stream().map(varName -> new TypeVariable(varName)).collect(Collectors.toList()));
        }
        resultType = new TypeSymbol(namedAstType.getName(),subtypes);
        if(namedAstType.getTypeArgs().length == 0){
            if(typeParams.contains(namedAstType.getName())){
                resultType = new TypeVariable(namedAstType.getName());
            }else if(procedureTypeParams.contains(namedAstType.getName())){
                resultType = new TypeVariable(namedAstType.getName());
            }
        }
    }


    /**
     * translation of inbuilt type int, real, bool
     * @param primitiveAstType
     */
    @Override
    public void visit(PrimitiveAstType primitiveAstType) {
        resultType = new TypeSymbol(primitiveAstType.getName());
    }
}
