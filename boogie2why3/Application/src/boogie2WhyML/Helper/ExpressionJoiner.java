package boogie2WhyML.Helper;

import whyML.ast.expression.Expression;
import whyML.ast.expression.InfixExpression;
import whyML.ast.formula.BinaryFormula;
import whyML.ast.formula.Formula;

import java.util.List;


public class ExpressionJoiner {


    /**
     * creates a conjunction of several expressions.
     * (((e1 && e2) && e3) && e4) ...
     * @param useAsymmetricConjunction if true, it uses &&, otherwise /\
     * @return conjunction of all expressions in the list. null if list is empty or null
     */
    public static Formula conjunction(List<Formula> expressionsList, boolean useAsymmetricConjunction){
        if(expressionsList == null || expressionsList.isEmpty()){
            return null;
        }
        String conjunctionSymbol;
        if(useAsymmetricConjunction){
            conjunctionSymbol = "&&";
        }else{
            conjunctionSymbol = "/\\";
        }
        //list has at least 1 element.
        Formula resultExpression = expressionsList.get(0);
        for(int i = 1; i < expressionsList.size(); ++i){
            resultExpression = new BinaryFormula(resultExpression, conjunctionSymbol, expressionsList.get(i));
        }
        return resultExpression;
    }


}
