package boogie2WhyML.Helper;


import boogie.ProgramFactory;
import boogie.ast.*;
import boogie.ast.asttypes.ArrayAstType;
import boogie.ast.asttypes.NamedAstType;
import boogie.ast.asttypes.PrimitiveAstType;
import boogie.ast.astvisitor.ASTVisitor;
import boogie.ast.declaration.*;
import boogie.ast.expression.*;
import boogie.ast.expression.literal.*;
import boogie.ast.specification.*;
import boogie.ast.statement.*;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class ContractGenerator implements ASTVisitor {


    private String currentProcedureName;

    private ProgramFactory programFactory;
    private Map<String,List<Specification>> procedureImplementationContracts;
    private Map<String,List<Specification>> abstractProcedureContracts;

    public List<Specification> getAbstractProcedureContracts(String procedureName) {
        return abstractProcedureContracts.get(procedureName);
    }

    public List<Specification> getProcedureImplementationContracts(String procedureName) {
        return procedureImplementationContracts.get(procedureName);
    }

    /**
     * generates contract specifications for procedures. The Abstract function needs free postconditions to be included, but NOT
     * free pre conditions. the function implementation needs the opposite. This class generates list of contracts for both.
     * It does  NOT skip modifies specifications (anymore).
     * @param programFactory
     */
    public ContractGenerator(ProgramFactory programFactory){
        procedureImplementationContracts = new LinkedHashMap<>();
        abstractProcedureContracts = new LinkedHashMap<>();
        this.programFactory = programFactory;

        for(Declaration d: programFactory.getASTRoot().getDeclarations()){
            d.accept(this);
        }
        List<Specification> result = new LinkedList<>();
    }



    @Override
    public void visit(BitvecLiteral bitvecLiteral) {

    }

    @Override
    public void visit(BooleanLiteral booleanLiteral) {

    }

    @Override
    public void visit(IntegerLiteral integerLiteral) {

    }

    @Override
    public void visit(RealLiteral realLiteral) {

    }

    @Override
    public void visit(StringLiteral stringLiteral) {

    }

    @Override
    public void visit(ArrayAccessExpression arrayAccessExpression) {

    }

    @Override
    public void visit(ArrayStoreExpression arrayStoreExpression) {

    }

    @Override
    public void visit(BinaryExpression binaryExpression) {

    }

    @Override
    public void visit(BitVectorAccessExpression bitVectorAccessExpression) {

    }

    @Override
    public void visit(CodeExpression codeExpression) {

    }

    @Override
    public void visit(FunctionApplication functionApplication) {

    }

    @Override
    public void visit(IdentifierExpression identifierExpression) {

    }

    @Override
    public void visit(IfThenElseExpression ifThenElseExpression) {

    }

    @Override
    public void visit(QuantifierExpression quantifierExpression) {

    }

    @Override
    public void visit(UnaryExpression unaryExpression) {

    }

    @Override
    public void visit(WildcardExpression wildcardExpression) {

    }

    @Override
    public void visit(EnsuresSpecification ensuresSpecification) {
        abstractProcedureContracts.get(currentProcedureName).add(ensuresSpecification);
        if(!ensuresSpecification.isFree()){
            procedureImplementationContracts.get(currentProcedureName).add(ensuresSpecification);
        }
    }

    @Override
    public void visit(LoopInvariantSpecification loopInvariantSpecification) {

    }

    @Override
    public void visit(ModifiesSpecification modifiesSpecification) {
        abstractProcedureContracts.get(currentProcedureName).add(modifiesSpecification);
        //TODO we might give an option to add it here too, so Why3 can check what is not in the read clause..
        //procedureImplementationContracts.get(currentProcedureName).add(modifiesSpecification);
    }

    @Override
    public void visit(RequiresSpecification requiresSpecification) {
        procedureImplementationContracts.get(currentProcedureName).add(requiresSpecification);
        if(!requiresSpecification.isFree()){
            abstractProcedureContracts.get(currentProcedureName).add(requiresSpecification);
        }
    }

    @Override
    public void visit(AssertStatement assertStatement) {

    }

    @Override
    public void visit(AssignmentStatement assignmentStatement) {

    }

    @Override
    public void visit(AssumeStatement assumeStatement) {

    }

    @Override
    public void visit(BreakStatement breakStatement) {

    }

    @Override
    public void visit(CallStatement callStatement) {

    }

    @Override
    public void visit(GotoStatement gotoStatement) {

    }

    @Override
    public void visit(HavocStatement havocStatement) {

    }

    @Override
    public void visit(IfStatement ifStatement) {

    }

    @Override
    public void visit(Label label) {

    }

    @Override
    public void visit(ParallelCall parallelCall) {

    }

    @Override
    public void visit(ReturnStatement returnStatement) {

    }

    @Override
    public void visit(WhileStatement whileStatement) {

    }

    @Override
    public void visit(YieldStatement yieldStatement) {

    }

    @Override
    public void visit(ArrayLHS arrayLHS) {

    }

    @Override
    public void visit(Body body) {

    }

    @Override
    public void visit(NamedAttribute namedAttribute) {

    }

    @Override
    public void visit(ParentEdge parentEdge) {

    }

    @Override
    public void visit(Project project) {

    }

    @Override
    public void visit(Trigger trigger) {

    }

    @Override
    public void visit(Unit unit) {

    }

    @Override
    public void visit(VariableLHS variableLHS) {

    }

    @Override
    public void visit(VarList varList) {

    }

    @Override
    public void visit(Axiom axiom) {

    }

    @Override
    public void visit(ConstDeclaration constDeclaration) {

    }

    @Override
    public void visit(FunctionDeclaration functionDeclaration) {

    }

    @Override
    public void visit(Implementation implementation) {

    }

    @Override
    public void visit(ProcedureDeclaration procedureDeclaration) {
        currentProcedureName = procedureDeclaration.getIdentifier();
        abstractProcedureContracts.put(currentProcedureName,new LinkedList<>());
        procedureImplementationContracts.put(currentProcedureName,new LinkedList<>());
        Specification[] contracts = procedureDeclaration.getSpecification();
        for(Specification specification: contracts){
            specification.accept(this);
        }


    }

    @Override
    public void visit(TypeDeclaration typeDeclaration) {

    }

    @Override
    public void visit(VariableDeclaration variableDeclaration) {

    }

    @Override
    public void visit(ArrayAstType arrayAstType) {

    }

    @Override
    public void visit(NamedAstType namedAstType) {

    }

    @Override
    public void visit(PrimitiveAstType primitiveAstType) {

    }
}
