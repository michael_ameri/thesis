package boogie2WhyML.Helper.VariableTypeParam;

import boogie.ProgramFactory;
import boogie.ast.*;
import boogie.ast.asttypes.ArrayAstType;
import boogie.ast.asttypes.NamedAstType;
import boogie.ast.asttypes.PrimitiveAstType;
import boogie.ast.astvisitor.ASTVisitor;
import boogie.ast.declaration.*;
import boogie.ast.expression.*;
import boogie.ast.expression.literal.*;
import boogie.ast.specification.EnsuresSpecification;
import boogie.ast.specification.LoopInvariantSpecification;
import boogie.ast.specification.ModifiesSpecification;
import boogie.ast.specification.RequiresSpecification;
import boogie.ast.statement.*;
import boogie.type.ArrayType;
import boogie.type.BoogieType;
import boogie.type.PlaceholderType;
import javafx.util.Pair;

import java.util.*;

/**
 * TODO: This class can be used in the future to translate map type with type variable of the form x:<a>[int]a
 */
public class VariableTypeEnumerator implements ASTVisitor{


    private Set<ASTNode> processed = new HashSet<>();

    /**
     * process children of an AST node.
     * @param objects
     */
    private void process(Collection<Object> objects){
        for(Object o: objects){
            if(o instanceof ASTNode){
                ASTNode node = (ASTNode)o;
                if(!processed.contains(node)){
                    node.accept(this);
                    processed.add(node);
                }
            }else if(o instanceof Collection){
                process((Collection)o);
            }else if(o instanceof Object[]){
                //this works b/c Arrays.asList keep same object references
                //from java doc: (Changes to the returned list "write through" to the array.)
                process(Arrays.asList((Object[]) o));
            }
        }
    }


    private ProgramFactory programFactory;
    private List<String> allVariableNames;

    /**
     * TODO this is description for next class.
     * programFactory must be type checked.
     * replaces statements in programFactory with statements that are compatible for translation.
     *
     * e.g.
     *
     * val : <a> [a] bool;
     * ...
     * val[1] := true;
     * assume forall <a>x:a :: val[a] == true;
     * val[false] := false;
     *
     * ...
     *
     * is replaced by
     *
     * val1: [int]bool;
     * val2: [bool]bool;
     * ...
     *
     * val1[1] := true;
     * assume forall x1:int, x2:int :: val1[x1] == true && val2[x2] == true;
     * val2[false] := false;
     *
     * @param programFactory must be type checked.
     * @param globalVariableNames global variables which contain type params.
     * @param localVariableNames local variables which contain type params.
     */


    /**
     *
     * for each variable in the input, find all concrete instantiations.
     * e.g.
     *
     * val: <a>[a]int;
     *
     *
     * x := val[1];
     * val[true] := 4;
     *
     *
     * -> val1: [int]int
     *      val2: [bool]int;
     *
     * @param programFactory must be type checked.
     * @param globalVariableNames global variables which contain type params.
     * @param localVariableNames local variables which contain type params.
     */
    public VariableTypeEnumerator(ProgramFactory programFactory, List<String> globalVariableNames, List<String> localVariableNames){
        this.programFactory = programFactory;
        this.allVariableNames = new LinkedList<>();
        allVariableNames.addAll(globalVariableNames);
        allVariableNames.addAll(localVariableNames);
        concreteInstantiations = new LinkedHashMap<>();

        for(String s: allVariableNames){
            concreteInstantiations.put(s, new HashSet<>());
        }

        for(Declaration d: programFactory.getGlobalDeclarations()){
            d.accept(this);
        }

    }


    /**
     * map from a variable name, to a list of all concrete instantiations.
     */
    private Map<String,Set<BoogieType>> concreteInstantiations;

    public Map<String, Set<BoogieType>> getConcreteInstantiations() {
        return concreteInstantiations;
    }

    /**
     * map from a variable name and an instantiation, to the new name of the variable
     * e.g.
     *
     * <var, [int]bool> == var1;
     *
     */
    private Map<Pair<String,BoogieType>,String> instantiationNames = new LinkedHashMap<>();

    public String getInstantiationName(Pair<String,BoogieType> nameAndInstantiation) {
        return instantiationNames.get(nameAndInstantiation);
    }

    @Override
    public void visit(BitvecLiteral bitvecLiteral) {
        //nothing
    }

    @Override
    public void visit(BooleanLiteral booleanLiteral) {
        //nothing
    }

    @Override
    public void visit(IntegerLiteral integerLiteral) {
        //nothing
    }

    @Override
    public void visit(RealLiteral realLiteral) {
        //nothing
    }

    @Override
    public void visit(StringLiteral stringLiteral) {
        //nothing
    }

    @Override
    public void visit(ArrayAccessExpression arrayAccessExpression) {
        //TODO
        for(Expression e: arrayAccessExpression.getIndices()){
            e.accept(this);
        }
        if(arrayAccessExpression.getArray() instanceof IdentifierExpression){
            IdentifierExpression identifierExpression = (IdentifierExpression)arrayAccessExpression.getArray();
            if(allVariableNames.contains(identifierExpression.getIdentifier()) ){
                //fixme if the type is NOT an arraytype, how to handle? e.g. var x: Field <a>[a]int (it's not relevant for autoproof so ignored for now.)
                ArrayType arrayType = (ArrayType)identifierExpression.getType().getUnderlyingType();
                BoogieType[]indexTypes = new BoogieType[arrayAccessExpression.getIndices().length];
                for(int i = 0; i < indexTypes.length; ++i){
                    indexTypes[i] = arrayAccessExpression.getIndices()[i].getType().getUnderlyingType();
                    if(indexTypes[i] instanceof PlaceholderType){
                        //if we have a placeholdertype, it is not concrete, so we do not add it.
                        return;
                    }
                }

                BoogieType valueType = arrayAccessExpression.getType().getUnderlyingType();
                if(valueType instanceof PlaceholderType){
                    return;
                }
                //fixme make constructor public
                BoogieType boogieType = new ArrayType(0,indexTypes,valueType);
                //BoogieType boogieType = null;
                concreteInstantiations.get(identifierExpression.getIdentifier()).add(boogieType);
            }
        }else{
            arrayAccessExpression.getArray().accept(this);
        }
    }

    @Override
    public void visit(ArrayStoreExpression arrayStoreExpression) {
        //todo copied from ArrayAccessExpression...
        for(Expression e: arrayStoreExpression.getIndices()){
            e.accept(this);
        }
        if(arrayStoreExpression.getArray() instanceof IdentifierExpression){
            IdentifierExpression identifierExpression = (IdentifierExpression)arrayStoreExpression.getArray();
            if(allVariableNames.contains(identifierExpression.getIdentifier()) ){
                //fixme if the type is NOT an arraytype, how to handle? e.g. var x: Field <a>[a]int (it's not relevant for autoproof so ignored for now.)
                ArrayType arrayType = (ArrayType)identifierExpression.getType().getUnderlyingType();
                BoogieType[]indexTypes = new BoogieType[arrayStoreExpression.getIndices().length];
                for(int i = 0; i < indexTypes.length; ++i){
                    indexTypes[i] = arrayStoreExpression.getIndices()[i].getType().getUnderlyingType();
                    if(indexTypes[i] instanceof PlaceholderType){
                        //if we have a placeholdertype, it is not concrete, so we do not add it.
                        return;
                    }
                }

                BoogieType valueType = arrayStoreExpression.getType().getUnderlyingType();
                if(valueType instanceof PlaceholderType){
                    return;
                }
                //fixme make constructor public
                //BoogieType boogieType = new ArrayType(0,indexTypes,valueType);
                BoogieType boogieType = null;
                concreteInstantiations.get(identifierExpression.getIdentifier()).add(boogieType);
            }
        }else{
            arrayStoreExpression.getArray().accept(this);
        }
    }

    @Override
    public void visit(BinaryExpression binaryExpression) {
        process(binaryExpression.getChildren());
    }

    @Override
    public void visit(BitVectorAccessExpression bitVectorAccessExpression) {
        //nothing
    }

    @Override
    public void visit(CodeExpression codeExpression) {
        //nothing
    }

    @Override
    public void visit(FunctionApplication functionApplication) {
        process(functionApplication.getChildren());
    }

    @Override
    public void visit(IdentifierExpression identifierExpression) {
        //nothing
        //TODO correct?
    }

    @Override
    public void visit(IfThenElseExpression ifThenElseExpression) {
        process(ifThenElseExpression.getChildren());
    }

    @Override
    public void visit(QuantifierExpression quantifierExpression) {
        //TODO correct? no need to save what bound variables are of general type e.g.?
        process(quantifierExpression.getChildren());
    }

    @Override
    public void visit(UnaryExpression unaryExpression) {
        process(unaryExpression.getChildren());
    }

    @Override
    public void visit(WildcardExpression wildcardExpression) {

    }

    @Override
    public void visit(EnsuresSpecification ensuresSpecification) {
        process(ensuresSpecification.getChildren());
    }

    @Override
    public void visit(LoopInvariantSpecification loopInvariantSpecification) {
        process(loopInvariantSpecification.getChildren());
    }

    @Override
    public void visit(ModifiesSpecification modifiesSpecification) {
        process(modifiesSpecification.getChildren());
    }

    @Override
    public void visit(RequiresSpecification requiresSpecification) {
        process(requiresSpecification.getChildren());
    }

    @Override
    public void visit(AssertStatement assertStatement) {
        process(assertStatement.getChildren());
    }

    @Override
    public void visit(AssignmentStatement assignmentStatement) {
        process(assignmentStatement.getChildren());
    }

    @Override
    public void visit(AssumeStatement assumeStatement) {
        process(assumeStatement.getChildren());
    }

    @Override
    public void visit(BreakStatement breakStatement) {
        //nothing
    }

    @Override
    public void visit(CallStatement callStatement) {
        process(callStatement.getChildren());
    }

    @Override
    public void visit(GotoStatement gotoStatement) {
        //nothing
    }

    @Override
    public void visit(HavocStatement havocStatement) {
        process(havocStatement.getChildren());
    }

    @Override
    public void visit(IfStatement ifStatement) {
        process(ifStatement.getChildren());
    }

    @Override
    public void visit(Label label) {
        //nothing
    }

    @Override
    public void visit(ParallelCall parallelCall) {
        process(parallelCall.getChildren());
    }

    @Override
    public void visit(ReturnStatement returnStatement) {
        process(returnStatement.getChildren());
    }

    @Override
    public void visit(WhileStatement whileStatement) {
        process(whileStatement.getChildren());
    }

    @Override
    public void visit(YieldStatement yieldStatement) {
        //todo
    }

    @Override
    public void visit(ArrayLHS arrayLHS) {
        //todo copied from ArrayAccessExpression...
        for(Expression e: arrayLHS.getIndices()){
            e.accept(this);
        }
        if(arrayLHS.getArray() instanceof VariableLHS){
            VariableLHS variableLHS = (VariableLHS)arrayLHS.getArray();
            if(allVariableNames.contains(variableLHS.getIdentifier()) ){
                //fixme if the type is NOT an arraytype, how to handle? e.g. var x: Field <a>[a]int (it's not relevant for autoproof so ignored for now.)
                ArrayType arrayType = (ArrayType)variableLHS.getType().getUnderlyingType();
                BoogieType[]indexTypes = new BoogieType[arrayLHS.getIndices().length];
                for(int i = 0; i < indexTypes.length; ++i){
                    indexTypes[i] = arrayLHS.getIndices()[i].getType().getUnderlyingType();
                    if(indexTypes[i] instanceof PlaceholderType){
                        //if we have a placeholdertype, it is not concrete, so we do not add it.
                        return;
                    }
                }

                BoogieType valueType = arrayLHS.getType().getUnderlyingType();
                if(valueType instanceof PlaceholderType){
                    return;
                }
                //fixme make constructor public
                //BoogieType boogieType = new ArrayType(0,indexTypes,valueType);
                BoogieType boogieType = null;
                concreteInstantiations.get(variableLHS.getIdentifier()).add(boogieType);
            }
        }else{
            arrayLHS.getArray().accept(this);
        }
    }

    @Override
    public void visit(Body body) {
        process(body.getChildren());
    }

    @Override
    public void visit(NamedAttribute namedAttribute) {
        //nothing
    }

    @Override
    public void visit(ParentEdge parentEdge) {
        //TODO nothing
    }

    @Override
    public void visit(Project project) {
        //TODO
    }

    @Override
    public void visit(Trigger trigger) {
        process(trigger.getChildren());
    }

    @Override
    public void visit(Unit unit) {
        process(unit.getChildren());
    }

    @Override
    public void visit(VariableLHS variableLHS) {
        //fixme
    }

    @Override
    public void visit(VarList varList) {
        //FIXME
    }

    @Override
    public void visit(Axiom axiom) {
        process(axiom.getChildren());
    }

    @Override
    public void visit(ConstDeclaration constDeclaration) {
        //todo nothing
    }

    @Override
    public void visit(FunctionDeclaration functionDeclaration) {
        process(functionDeclaration.getChildren());
    }

    @Override
    public void visit(Implementation implementation) {
        //fixme problem if two different implementations have local variables with the same name
        process(implementation.getChildren());
    }

    @Override
    public void visit(ProcedureDeclaration procedureDeclaration) {
        //fixme problem if two different implementations have local variables with the same name
        process(procedureDeclaration.getChildren());
    }

    @Override
    public void visit(TypeDeclaration typeDeclaration) {
        //nothing
    }

    @Override
    public void visit(VariableDeclaration variableDeclaration) {
        //nothing
    }

    @Override
    public void visit(ArrayAstType arrayAstType) {
        //nothing
    }

    @Override
    public void visit(NamedAstType namedAstType) {
        //nothing
    }

    @Override
    public void visit(PrimitiveAstType primitiveAstType) {
        //nothing
    }
}
