package ch.ethz.ameri.main;

import boogie.ProgramFactory;
import boogie2WhyML.BoogieTranslator;
import boogie2WhyML.PreambleGenerator;
import org.apache.log4j.Level;
import util.Log;
import whyML.ast.Module;
import whyML.ast.declerations.Declaration;
import whyML.astvisitor.ModulePrinterVisitor;

import java.util.Arrays;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        //TODO use argparse4j or similar library instead of parsing input manually.
        if(args.length < 1 || Arrays.asList(args).contains("-h")){
            printHelp();
            return;
        }
        if(!Arrays.asList(args).contains("-debug")){
            Log.v().setLevel(Level.OFF);
        }
        String boogieFileName = args[0];
        String translationOutputFile = null;
        if(args.length >= 2 && !"-debug".equals(args[1])){
            translationOutputFile = args[1];
        }

        //create a (legal WhyML module) file containing just the preamble.
        Module module = new Module("Preamble", "this module contains the preamble which is present in every translation");
        module.addDeclerations(PreambleGenerator.generatePreamble());

        ModulePrinterVisitor mpv = new ModulePrinterVisitor(module);
        List<String> printedModule = mpv.printModuleToString();

        //start actual translation
        ProgramFactory pf;
        try {
            pf = new ProgramFactory(boogieFileName);
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }
        Log.info("Programfactory created.");

        Log.info("Translate declarations...");
        Module translationModule = new Module("Translation", "This translation was automatically generated.");
        BoogieTranslator bt = new BoogieTranslator();
        //no renaming, it is done on the Boogie AST now.
        List<Declaration> translatedDeclarations = bt.translate(pf,true,false,true);
        translationModule.addDeclerations(translatedDeclarations);
        ModulePrinterVisitor mpvTranslation = new ModulePrinterVisitor(translationModule);
        List<String> printableModuleTranslation = mpvTranslation.printModuleToString();
        //print to file or console.
        ModulePrinterVisitor.print(printableModuleTranslation, translationOutputFile);
        Log.info("Translation Done.");
    }


    /**
     * outputs help text to console.
     */
    public static void printHelp(){
        System.out.println("1. Argument: input file");
        System.out.println("(Optional) 2. Argument: output file. Otherwise output is printed to console.");
        System.out.println("Optional further arguments:");
        System.out.println("\"-q\" to disable warnings and information on console (quiet)");
        System.out.println("\"-h\" to display (this) help");
    }

}
