package whyML.ast.types;


import whyML.ast.ASTNode;

public abstract class Type implements ASTNode {//TODO could also be change to implement ASTNode, and use visitor pattern. Would that be better though?
    public abstract String getTypeName();

    /**
     * add "ref" to the beginning of the type.
     * e.g. a -> ref (a)
     * map int int -> ref (map int int)
     */
    public abstract void addRef();
}
