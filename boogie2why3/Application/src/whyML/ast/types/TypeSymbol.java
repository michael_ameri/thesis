package whyML.ast.types;


import whyML.ast.ASTNode;
import whyML.astvisitor.ASTVisitor;

import java.util.LinkedList;
import java.util.List;

public class TypeSymbol extends Type {

    private String lqualid;
    //can be an empty list
    private List<Type> types;
    private boolean isRef;

    @Override
    public List<ASTNode> getChildren() {
        List<ASTNode> children = new LinkedList<>();
        children.addAll(types);
        return children;
    }

    public TypeSymbol(String lqualid, List<Type> types){
        this.lqualid = lqualid;
        this.types = types;
        isRef = false;
    }

    public TypeSymbol(String lqualid){
        this.lqualid = lqualid;
        this.types =  new LinkedList<Type>();
        isRef = false;
    }

    @Override
    public String getTypeName() {
        String res = "";
        res = res.concat(lqualid+" ");
        if(types != null){
            for(Type s: types){
                res = res.concat(s.getTypeName()); //TODO makes a lot of parentheses, make sure it's legal!
            }
        }
        if(res.trim().contains(" ")){
            res = "(" + res.trim() + ") ";
        }
        if(isRef){
            res = "(ref " + res + ")";
        }
        return res;
    }

    @Override
    public void addRef() {
        isRef = true;
    }

    public String getLqualid() {
        return lqualid;
    }

    public void setLqualid(String lqualid) {
        this.lqualid = lqualid;
    }

    public List<Type> getTypes() {
        return types;
    }

    public void setSubTypeAt(int index, Type element) {
        this.types.set(index,element);
    }

    public void addSubType(Type element){
        this.types.add(element);
    }

    @Override
    public void accept(ASTVisitor visitor) {
        visitor.visit(this);
    }
}
