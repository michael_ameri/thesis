package whyML.ast.types;


import whyML.ast.ASTNode;
import whyML.astvisitor.ASTVisitor;

import java.util.LinkedList;
import java.util.List;

public class TypeVariable extends Type {

    private String lqualid;
    private boolean isRef;

    //Type variables, e.g. of the form: 't
    public TypeVariable(String lqualid){
        this.lqualid = lqualid;
        this.isRef = false;
    }

    @Override
    public String getTypeName() {
        if(isRef){
            return "(ref '"+lqualid+") ";
        }else{
            return "'"+lqualid+" ";
        }

    }

    /**
     *
     * @return the lqualid part, WITHOUT the ' before.
     */
    public String getLqualid() {
        return lqualid;
    }

    public void setLqualid(String lqualid) {
        this.lqualid = lqualid;
    }

    @Override
    public void addRef() {
        isRef = true;
    }

    @Override
    public void accept(ASTVisitor visitor) {
        visitor.visit(this);
    }

    @Override
    public List<ASTNode> getChildren() {
        return new LinkedList<>();
    }
}
