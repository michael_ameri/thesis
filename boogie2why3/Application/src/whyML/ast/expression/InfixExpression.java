package whyML.ast.expression;

import whyML.ast.ASTNode;
import whyML.astvisitor.ASTVisitor;

import java.util.LinkedList;
import java.util.List;


public class InfixExpression extends Expression {

    private Expression leftExpression;
    private Expression rightExpression;
    private String infixOperator;

    public InfixExpression(Expression leftExpression, String infixOperator, Expression rightExpression) {
        this.leftExpression = leftExpression;
        this.rightExpression = rightExpression;
        this.infixOperator = infixOperator;
    }

    public Expression getLeftExpression() {
        return leftExpression;
    }

    public Expression getRightExpression() {
        return rightExpression;
    }

    public String getInfixOperator() {
        return infixOperator;
    }

    @Override
    public void accept(ASTVisitor visitor) {
        visitor.visit(this);
    }

    @Override
    public List<ASTNode> getChildren() {
        List<ASTNode> children = new LinkedList<>();
        children.add(leftExpression);
        children.add(rightExpression);
        return children;
    }
}
