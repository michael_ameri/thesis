package whyML.ast.expression;

import whyML.ast.ASTNode;
import whyML.ast.formula.Formula;
import whyML.astvisitor.ASTVisitor;

import java.util.LinkedList;
import java.util.List;


public class AssertionExpression extends Expression {

    private Formula formula;

    //assert, check or assume
    private AssertionType assertionType;


    @Override
    public List<ASTNode> getChildren() {
        List<ASTNode> children = new LinkedList<>();
        children.add(formula);
        return children;
    }

    /**
     * if assertionType is "absurd", then formula is set to null.
     * @param assertionType
     * @param formula
     */
    public AssertionExpression(AssertionType assertionType, Formula formula){
        this.assertionType = assertionType;
        if(this.assertionType == AssertionType.ABSURD){
            this.formula = null;
        }else{
            this.formula = formula;
        }
    }

    @Override
    public void accept(ASTVisitor visitor) {
        visitor.visit(this);
    }

    public Formula getFormula() {
        return formula;
    }

    public AssertionType getAssertionType() {
        return assertionType;
    }

    public enum AssertionType{
        ASSERT("assert"),
        CHECK("check"),
        ASSUME("assume"),
        ABSURD("absurd")
        ;

        private final String text;

        /**
         * @param text
         */
        private AssertionType(final String text) {
            this.text = text;
        }

        /* (non-Javadoc)
         * @see java.lang.Enum#toString()
         */
        @Override
        public String toString() {
            return text;
        }
    }


}



