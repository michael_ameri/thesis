package whyML.ast.expression;


import whyML.ast.ASTNode;
import whyML.astvisitor.ASTVisitor;

import java.util.LinkedList;
import java.util.List;

public class ExceptionCatchingExpression extends Expression{

    Expression expression;
    //must contain at least 1 to be valid syntax.
    List<Handler> handlers;


    public ExceptionCatchingExpression(Expression expression, List<Handler> handlers){
        this.expression = expression;
        this.handlers = handlers;
    }

    public ExceptionCatchingExpression(Expression expression, Handler handler){
        this.expression = expression;
        this.handlers = new LinkedList<Handler>();
        this.handlers.add(handler);
    }

    public Expression getExpression() {
        return expression;
    }

    public List<Handler> getHandlers() {
        return handlers;
    }

    @Override
    public void accept(ASTVisitor visitor) {
        visitor.visit(this);
    }

    @Override
    public List<ASTNode> getChildren() {
        List<ASTNode> children = new LinkedList<>();
        children.add(expression);
        children.addAll(handlers);
        return children;
    }
}
