package whyML.ast.expression;


import whyML.ast.ASTNode;
import whyML.astvisitor.ASTVisitor;

import java.util.LinkedList;
import java.util.List;

public class SequenceExpression extends Expression{

    private Expression exp1;
    private Expression exp2;

    public SequenceExpression(Expression exp1, Expression exp2){
        this.exp1 = exp1;
        this.exp2 = exp2;
    }

    public Expression getExp1() {
        return exp1;
    }

    public Expression getExp2() {
        return exp2;
    }

    public void setExp1(Expression exp1) {
        this.exp1 = exp1;
    }

    public void setExp2(Expression exp2) {
        this.exp2 = exp2;
    }

    @Override
    public void accept(ASTVisitor visitor) {
        visitor.visit(this);
    }

    @Override
    public List<ASTNode> getChildren() {
        List<ASTNode> children = new LinkedList<>();
        children.add(exp1);
        children.add(exp2);
        return children;
    }
}
