package whyML.ast.expression;


import whyML.ast.ASTNode;
import whyML.ast.pattern.Pattern;
import whyML.astvisitor.ASTVisitor;

import java.util.LinkedList;
import java.util.List;

/**
 * "let pattern = boundExpr in nextExpr"
 */
public class LocalBindingExpression extends Expression{



    private Pattern pattern;
    private Expression boundExpr;
    private Expression nextExpr;

    /**
     * "let pattern = boundExpr in nextExpr"
     * @param pattern
     * @param boundExpr
     * @param nextExpr
     */
    public LocalBindingExpression(Pattern pattern, Expression boundExpr, Expression nextExpr){
        this.pattern = pattern;
        this.boundExpr = boundExpr;
        this.nextExpr = nextExpr;
    }

    public Expression getBoundExpr() {
        return boundExpr;
    }

    public Expression getNextExpr() {
        return nextExpr;
    }

    public Pattern getPattern() {
        return pattern;
    }

    public void setNextExpr(Expression nextExpr) {
        this.nextExpr = nextExpr;
    }

    public void setBoundExpr(Expression boundExpr) {
        this.boundExpr = boundExpr;
    }

    public void setPattern(Pattern pattern) {
        this.pattern = pattern;
    }

    @Override
    public void accept(ASTVisitor visitor) {
        visitor.visit(this);
    }

    @Override
    public List<ASTNode> getChildren() {
        List<ASTNode> children = new LinkedList<>();
        children.add(pattern);
        children.add(boundExpr);
        children.add(nextExpr);
        return children;
    }
}
