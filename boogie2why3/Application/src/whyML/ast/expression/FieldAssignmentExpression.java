package whyML.ast.expression;

import whyML.ast.ASTNode;
import whyML.astvisitor.ASTVisitor;

import java.util.LinkedList;
import java.util.List;


public class FieldAssignmentExpression extends Expression {

    private Expression firstExpression;
    //lqualid can be null.
    private String lqualid;
    private Expression secondExpression;

    /**
     * firstExpression.lqualid <- secondExpression
     * @param firstExpression
     * @param lqualid can be null. Then the second part of the lhs must be contained in firstExpression.
     * @param secondExpression
     */
    public FieldAssignmentExpression(Expression firstExpression, String lqualid, Expression secondExpression){
        this.firstExpression = firstExpression;
        this.secondExpression = secondExpression;
        this.lqualid = lqualid;
    }

    public Expression getFirstExpression() {
        return firstExpression;
    }

    public Expression getSecondExpression() {
        return secondExpression;
    }

    public String getLqualid() {
        return lqualid;
    }

    public void setLqualid(String lqualid) {
        this.lqualid = lqualid;
    }

    @Override
    public void accept(ASTVisitor visitor) {
        visitor.visit(this);
    }

    @Override
    public List<ASTNode> getChildren() {
        List<ASTNode> children = new LinkedList<>();
        children.add(firstExpression);
        children.add(secondExpression);
        return children;
    }
}
