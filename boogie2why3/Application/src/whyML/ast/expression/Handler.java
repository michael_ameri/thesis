package whyML.ast.expression;

import whyML.ast.ASTNode;
import whyML.ast.expression.Expression;
import whyML.ast.pattern.Pattern;
import whyML.astvisitor.ASTVisitor;

import java.util.LinkedList;
import java.util.List;

/**
 * used for processing stuff in catch clauses
 */
public class Handler implements ASTNode {
    private String uqualid;
    //pattern can be null.
    private Pattern pattern;
    private Expression expression;

    @Override
    public List<ASTNode> getChildren() {
        List<ASTNode> children = new LinkedList<>();
        if(pattern != null){
            children.add(pattern);
        }
        children.add(expression);
        return children;
    }

    /**
     * standard constructor
     * @param uqualid
     * @param pattern
     * @param expression
     */
    public Handler(String uqualid, Pattern pattern, Expression expression){
        this.uqualid = uqualid;
        this.pattern = pattern;
        this.expression = expression;
    }

    /**
     * sets pattern to null.
     * @param uqualid
     * @param expression
     */
    public Handler(String uqualid, Expression expression){
        this.uqualid = uqualid;
        this.pattern = null;
        this.expression = expression;
    }


    public Pattern getPattern() {
        return pattern;
    }

    public Expression getExpression() {
        return expression;
    }

    public String getUqualid() {
        return uqualid;
    }

    public void setUqualid(String uqualid) {
        this.uqualid = uqualid;
    }

    @Override
    public void accept(ASTVisitor visitor) {
        visitor.visit(this);
    }
}
