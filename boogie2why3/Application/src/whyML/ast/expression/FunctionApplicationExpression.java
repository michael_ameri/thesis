package whyML.ast.expression;

import whyML.ast.ASTNode;
import whyML.astvisitor.ASTVisitor;

import java.util.LinkedList;
import java.util.List;


public class FunctionApplicationExpression extends Expression {

    //the function identifier usually
    private Expression firstExpression;
    //must contain at least 1 argument (can be ())
    private List<Expression> arguments;

    public FunctionApplicationExpression(Expression firstExpression, List<Expression> arguments){
        this.firstExpression = firstExpression;
        this.arguments = arguments;
    }

    public FunctionApplicationExpression(Expression firstExpression, Expression argument){
        this.firstExpression = firstExpression;
        this.arguments = new LinkedList<>();
        arguments.add(argument);
    }

    public Expression getFirstExpression() {
        return firstExpression;
    }

    public List<Expression> getArguments() {
        return arguments;
    }

    @Override
    public void accept(ASTVisitor visitor) {
        visitor.visit(this);
    }

    @Override
    public List<ASTNode> getChildren() {
        List<ASTNode> children = new LinkedList<>();
        children.add(firstExpression);
        children.addAll(arguments);
        return children;
    }
}
