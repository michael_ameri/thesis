package whyML.ast.terms;

import whyML.ast.ASTNode;
import whyML.astvisitor.ASTVisitor;

import java.util.LinkedList;
import java.util.List;

public class FunctionApplicationTerm extends Term{

    private String lqualid;
    //at least 1 term according to syntax.
    private List<Term> terms;

    public FunctionApplicationTerm(String lqualid, List<Term> terms){
        this.lqualid = lqualid;
        this.terms = terms;
    }

    public FunctionApplicationTerm(String lqualid, Term term){
        this.lqualid = lqualid;
        this.terms = new LinkedList<Term>();
        terms.add(term);
    }

    public List<Term> getTerms() {
        return terms;
    }

    public String getLqualid() {
        return lqualid;
    }

    public void setLqualid(String lqualid) {
        this.lqualid = lqualid;
    }

    @Override
    public void accept(ASTVisitor visitor) {
        visitor.visit(this);
    }

    @Override
    public List<ASTNode> getChildren() {
        List<ASTNode> children = new LinkedList<>();
        children.addAll(terms);
        return children;
    }
}
