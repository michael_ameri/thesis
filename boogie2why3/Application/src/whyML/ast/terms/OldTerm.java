package whyML.ast.terms;

import whyML.ast.ASTNode;
import whyML.astvisitor.ASTVisitor;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by Michael on 6/17/2015.
 */
public class OldTerm extends Term{
    private Term subTerm;

    public OldTerm(Term subTerm) {
        this.subTerm = subTerm;
    }

    public Term getSubTerm() {
        return subTerm;
    }

    @Override
    public void accept(ASTVisitor visitor) {
        visitor.visit(this);
    }

    @Override
    public List<ASTNode> getChildren() {
        List<ASTNode> children = new LinkedList<>();
        children.add(subTerm);
        return children;
    }
}
