package whyML.ast.terms;

import whyML.ast.ASTNode;
import whyML.astvisitor.ASTVisitor;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by Michael on 6/17/2015.
 */
public class PrefixTerm extends Term {

    private String infixOperator;
    private Term subTerm;

    public PrefixTerm(String infixOperator, Term subTerm) {
        this.infixOperator = infixOperator;
        this.subTerm = subTerm;
    }

    public String getInfixOperator() {
        return infixOperator;
    }

    public Term getSubTerm() {
        return subTerm;
    }

    @Override
    public void accept(ASTVisitor visitor) {
        visitor.visit(this);
    }

    @Override
    public List<ASTNode> getChildren() {
        List<ASTNode> children = new LinkedList<>();
        children.add(subTerm);
        return children;
    }
}
