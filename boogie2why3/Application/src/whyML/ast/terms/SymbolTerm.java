package whyML.ast.terms;


import whyML.ast.ASTNode;
import whyML.astvisitor.ASTVisitor;

import java.util.LinkedList;
import java.util.List;

public class SymbolTerm extends Term{

    private String lqualid;

    public SymbolTerm(String lqualid){
        this.lqualid = lqualid;
    }


    @Override
    public void accept(ASTVisitor visitor) {
        visitor.visit(this);
    }

    public String getLqualid() {
        return lqualid;
    }

    public void setLqualid(String lqualid) {
        this.lqualid = lqualid;
    }

    @Override
    public List<ASTNode> getChildren() {
        return new LinkedList<>();
    }
}
