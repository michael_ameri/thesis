package whyML.ast.declerations;


import whyML.ast.ASTNode;
import whyML.astvisitor.ASTVisitor;

import java.util.LinkedList;
import java.util.List;

public class ExceptionDeclaration extends Declaration {

    private String uident;
    //TODO WhyML syntax allows types as part of the declaration, but I don't need it for now.

    public ExceptionDeclaration(String uident){
        this.uident = uident;
    }

    @Override
    public void accept(ASTVisitor visitor) {
        visitor.visit(this);
    }

    public String getUident() {
        return uident;
    }

    public void setUident(String uident) {
        this.uident = uident;
    }

    @Override
    public List<ASTNode> getChildren() {
        return new LinkedList<>();
    }
}
