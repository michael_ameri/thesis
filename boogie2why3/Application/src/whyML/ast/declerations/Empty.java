package whyML.ast.declerations;


import whyML.ast.ASTNode;
import whyML.astvisitor.ASTVisitor;

import java.util.LinkedList;
import java.util.List;

public class Empty extends Declaration {
    @Override
    public void accept(ASTVisitor visitor) {
        visitor.visit(this);
    }

    @Override
    public List<ASTNode> getChildren() {
        return new LinkedList<>();
    }
}
