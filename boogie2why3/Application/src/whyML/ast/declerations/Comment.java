package whyML.ast.declerations;


import whyML.ast.ASTNode;
import whyML.astvisitor.ASTVisitor;

import java.util.LinkedList;
import java.util.List;

public class Comment extends Declaration {

    private String comment;

    public Comment(String comment){
        this.comment =  comment;
    }


    @Override
    public void accept(ASTVisitor visitor) {
        visitor.visit(this);
    }

    public String getComment(){
        return comment;
    }

    @Override
    public String toString() {
        return "(* " + comment + " *)";
    }

    @Override
    public List<ASTNode> getChildren() {
        return new LinkedList<>();
    }
}
