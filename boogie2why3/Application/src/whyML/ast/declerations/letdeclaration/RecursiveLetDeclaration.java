package whyML.ast.declerations.letdeclaration;


import whyML.ast.ASTNode;
import whyML.ast.Param;
import whyML.ast.declerations.Declaration;
import whyML.ast.expression.Expression;
import whyML.ast.spec.Spec;
import whyML.ast.types.Type;
import whyML.astvisitor.ASTVisitor;

import java.util.LinkedList;
import java.util.List;

public class RecursiveLetDeclaration extends Declaration {
    //let rec rec-defn
    private RecDefn recDefn;

    /**
     * general constructor
     * @param recDefn
     */
    public RecursiveLetDeclaration(RecDefn recDefn){
        this.recDefn = recDefn;
    }

    /**
     * constructor for easier syntax.
     * @param lident
     * @param inputParams
     * @param returnType
     * @param specs
     * @param expr
     */
    public RecursiveLetDeclaration(String lident, List<Param> inputParams, Type returnType, List<Spec> specs, Expression expr){
        FunBody funBody = new FunBody(inputParams,returnType,specs,expr);
        FunDefn funDefn = new FunDefn(lident,funBody);
        this.recDefn = new RecDefn(funDefn);
    }

    public RecDefn getRecDefn() {
        return recDefn;
    }

    @Override
    public void accept(ASTVisitor visitor) {
        visitor.visit(this);
    }

    @Override
    public List<ASTNode> getChildren() {
        List<ASTNode> children = new LinkedList<>();
        children.add(recDefn);
        return children;
    }
}
