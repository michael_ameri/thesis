package whyML.ast.declerations.letdeclaration;


import whyML.ast.ASTNode;
import whyML.ast.Label;
import whyML.astvisitor.ASTVisitor;

import java.util.LinkedList;
import java.util.List;

public class FunDefn implements ASTNode {
    private Boolean isGhost;
    private String lident;
    private Label label;
    private FunBody funBody;

    @Override
    public List<ASTNode> getChildren() {
        List<ASTNode> children = new LinkedList<>();
        children.add(label);
        children.add(funBody);
        return children;
    }

    public FunDefn(Boolean isGhost, String lident, Label label, FunBody funBody){
        this.isGhost = isGhost;
        this.lident = lident;
        this.label = label;
        this.funBody = funBody;
    }

    /**
     * NOT ghost  and NO label
     * @param lident
     * @param funBody
     */
    public FunDefn(String lident, FunBody funBody){
        this.isGhost = false;
        this.lident = lident;
        this.label = new Label(false);
        this.funBody = funBody;
    }

    public Boolean getIsGhost() {
        return isGhost;
    }

    public FunBody getFunBody() {
        return funBody;
    }

    public Label getLabel() {
        return label;
    }

    public String getLident() {
        return lident;
    }

    public void setLident(String lident) {
        this.lident = lident;
    }

    @Override
    public void accept(ASTVisitor visitor) {
        visitor.visit(this);
    }
}
