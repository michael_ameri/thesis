package whyML.ast.declerations;


import whyML.ast.ASTNode;
import whyML.ast.Label;
import whyML.ast.Param;
import whyML.ast.spec.Postcondition;
import whyML.ast.spec.Precondition;
import whyML.ast.spec.Spec;
import whyML.ast.types.Type;
import whyML.astvisitor.ASTVisitor;

import java.util.LinkedList;
import java.util.List;

public class ValAbstractFunction extends Val{

    private Boolean isGhost;
    private String name;
    private Label label;
    private Type returnType;
    private List<Param> inputArguments;

    private List<Spec> specs;



    public ValAbstractFunction(Boolean isGhost, String name, Label label, List<Param> inputArguments, Type returnType,
                               List<Precondition> preconditions, List<Postcondition> postconditions){
        this.isGhost = isGhost;
        this.name = name;
        this.label = label;
        this.returnType = returnType;
        this.inputArguments = inputArguments;
        this.specs = new LinkedList<Spec>();
        specs.addAll(postconditions);
        specs.addAll(preconditions);
    }

    /**
     * not gost and no label.
     * @param name
     * @param inputArguments
     * @param returnType
     * @param preconditions
     * @param postconditions
     */
    public ValAbstractFunction(String name, List<Param> inputArguments, Type returnType,
                               List<Precondition> preconditions, List<Postcondition> postconditions){
        this.isGhost = false;
        this.name = name;
        this.label = new Label(false);
        this.returnType = returnType;
        this.inputArguments = inputArguments;
        this.specs = new LinkedList<Spec>();
        specs.addAll(postconditions);
        specs.addAll(preconditions);
    }

    /**
     * not gost and no label.
     * @param name
     * @param inputArguments
     * @param returnType
     * @param specs
     */
    public ValAbstractFunction(String name, List<Param> inputArguments, Type returnType,
                               List<Spec> specs){
        this.isGhost = false;
        this.name = name;
        this.label = new Label(false);
        this.returnType = returnType;
        this.inputArguments = inputArguments;
        this.specs = specs;
    }

    /**
     * no pre or post conditions
     * @param isGhost
     * @param name
     * @param label
     * @param inputArguments
     * @param returnType
     */
    public ValAbstractFunction(Boolean isGhost, String name, Label label, List<Param> inputArguments, Type returnType){
        this.isGhost = isGhost;
        this.name = name;
        this.label = label;
        this.returnType = returnType;
        this.inputArguments = inputArguments;
        this.specs = new LinkedList<Spec>();
    }

    /**
     * abstract function without any input parameters and no contracts. e.g. val f(): int
     * @param isGhost
     * @param name
     * @param label
     * @param returnType
     */
    public ValAbstractFunction(Boolean isGhost, String name, Label label, Type returnType){
        this.isGhost = isGhost;
        this.name = name;
        this.label = label;
        this.returnType = returnType;
        this.inputArguments = new LinkedList<Param>();
        this.specs = new LinkedList<Spec>();
    }

    /**
     * abstract function without any input parameters, without label, and NOT ghost and no contracts. e.g. val f(): int.
     * @param name
     * @param returnType
     */
    public ValAbstractFunction( String name, Type returnType){
        this.isGhost = false;
        this.name = name;
        this.label = new Label(false);
        this.returnType = returnType;
        this.inputArguments = new LinkedList<Param>();
        this.specs = new LinkedList<Spec>();
    }

    /**
     * abstract function without label, and NOT ghost and no contracts.
     * @param name
     * @param returnType
     * @param inputArguments
     */
    public ValAbstractFunction(String name, List<Param> inputArguments, Type returnType){
        this.isGhost = false;
        this.name = name;
        this.label = new Label(false);
        this.returnType = returnType;
        this.inputArguments = inputArguments;
        this.specs = new LinkedList<Spec>();
    }

    public String getName() {
        return name;
    }

    public Label getLabel() {
        return label;
    }

    public Boolean getIsGhost() {
        return isGhost;
    }

    public List<Param> getInputArguments() {
        return inputArguments;
    }

    public Type getReturnType() {
        return returnType;
    }

    public List<Spec> getSpecs() {
        return specs;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public void accept(ASTVisitor visitor) {
        visitor.visit(this);
    }

    @Override
    public List<ASTNode> getChildren() {
        List<ASTNode> children = new LinkedList<>();
        children.add(label);
        children.add(returnType);
        children.addAll(inputArguments);
        children.addAll(specs);
        return children;
    }
}
