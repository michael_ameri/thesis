package whyML.ast.spec;


import whyML.ast.ASTNode;
import whyML.ast.formula.Formula;
import whyML.astvisitor.ASTVisitor;

import java.util.LinkedList;
import java.util.List;

public class Invariant extends Spec {
    private Formula formula;

    public Invariant(Formula formula){
        this.formula = formula;
    }

    @Override
    public void accept(ASTVisitor visitor) {
        visitor.visit(this);
    }

    public Formula getFormula() {
        return formula;
    }

    @Override
    public List<ASTNode> getChildren() {
        List<ASTNode> children = new LinkedList<>();
        children.add(formula);
        return children;
    }
}
