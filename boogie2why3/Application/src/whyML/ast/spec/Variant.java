package whyML.ast.spec;


import whyML.ast.ASTNode;
import whyML.ast.terms.Term;
import whyML.astvisitor.ASTVisitor;

import java.util.LinkedList;
import java.util.List;


public class Variant extends Spec{

    //TODO add variant-rel, which we don't need for the translation though.
    private List<Term> terms;

    public Variant(List<Term> terms){
        this.terms = terms;
    }

    public Variant(Term term){
        this.terms = new LinkedList<>();
        terms.add(term);
    }

    public List<Term> getTerms() {
        return terms;
    }

    @Override
    public void accept(ASTVisitor visitor) {
        visitor.visit(this);
    }

    @Override
    public List<ASTNode> getChildren() {
        //TODO variant-rel
        List<ASTNode> children = new LinkedList<>();
        children.addAll(terms);
        return children;
    }
}
