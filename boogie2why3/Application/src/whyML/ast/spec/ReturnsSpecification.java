package whyML.ast.spec;


import whyML.ast.ASTNode;
import whyML.ast.FormulaCase;
import whyML.ast.formula.Formula;
import whyML.ast.pattern.Pattern;
import whyML.astvisitor.ASTVisitor;

import java.util.LinkedList;
import java.util.List;

public class ReturnsSpecification extends Spec{

    //must contain at least 1 to be valid syntax
    private List<FormulaCase> formulaCases;

    public ReturnsSpecification(FormulaCase formulaCase){
        formulaCases = new LinkedList<FormulaCase>();
        formulaCases.add(formulaCase);
    }

    ReturnsSpecification(List<FormulaCase> formulaCases){
        this.formulaCases = formulaCases;
    }

    public List<FormulaCase> getFormulaCases() {
        return formulaCases;
    }

    @Override
    public void accept(ASTVisitor visitor) {
        visitor.visit(this);
    }

    @Override
    public List<ASTNode> getChildren() {
        List<ASTNode> children = new LinkedList<>();
        children.addAll(formulaCases);
        return children;
    }
}
