package whyML.ast;


import whyML.astvisitor.ASTVisitor;

import java.util.List;

public interface ASTNode {
    public void accept(ASTVisitor visitor);
    public List<ASTNode> getChildren();
}
