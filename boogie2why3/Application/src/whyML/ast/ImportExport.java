package whyML.ast;


public enum ImportExport {
    IMPORT,
    EXPORT,
    NONE
}
