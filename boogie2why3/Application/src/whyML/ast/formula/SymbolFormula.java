package whyML.ast.formula;


import whyML.ast.ASTNode;
import whyML.astvisitor.ASTVisitor;

import java.util.LinkedList;
import java.util.List;

public class SymbolFormula extends Formula{

    private String lqualid;

    public SymbolFormula(String lqualid){
        this.lqualid = lqualid;
    }

    @Override
    public void accept(ASTVisitor visitor) {
        visitor.visit(this);
    }

    public String getLqualid() {
        return lqualid;
    }

    public void setLqualid(String lqualid) {
        this.lqualid = lqualid;
    }

    @Override
    public List<ASTNode> getChildren() {
        return new LinkedList<>();
    }
}
