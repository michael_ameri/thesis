package whyML.ast.formula;


import whyML.ast.ASTNode;
import whyML.ast.terms.Term;
import whyML.astvisitor.ASTVisitor;

import java.util.LinkedList;
import java.util.List;

public class BinaryFormula extends Formula{
    private Formula leftFormula;
    private Formula rightFormula;

    //TODO operator enum.
    //->, <->, /\, &&, \/, ||
    private String operator;

    public BinaryFormula(Formula leftFormula, String operator, Formula rightFormula){
        this.leftFormula = leftFormula;
        this.operator = operator;
        this.rightFormula = rightFormula;
    }

    @Override
    public void accept(ASTVisitor visitor) {
        visitor.visit(this);
    }

    public Formula getLeftFormula() {
        return leftFormula;
    }

    public Formula getRightFormula() {
        return rightFormula;
    }

    public String getOperator() {
        return operator;
    }

    @Override
    public List<ASTNode> getChildren() {
        List<ASTNode> children = new LinkedList<>();
        children.add(leftFormula);
        children.add(rightFormula);
        return children;
    }
}
