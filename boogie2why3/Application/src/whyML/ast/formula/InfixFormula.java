package whyML.ast.formula;


import whyML.ast.ASTNode;
import whyML.ast.terms.Term;
import whyML.astvisitor.ASTVisitor;

import java.util.LinkedList;
import java.util.List;

public class InfixFormula extends Formula {

    private String infixOperator;//TODO could be replaced by enum.
    private Term leftTerm;
    private Term rightTerm;

    public InfixFormula(Term leftTerm, String infixOperator, Term rightTerm){
        this.leftTerm =leftTerm;
        this.infixOperator = infixOperator;
        this.rightTerm = rightTerm;
    }

    @Override
    public void accept(ASTVisitor visitor) {
        visitor.visit(this);
    }

    public String getInfixOperator() {
        return infixOperator;
    }

    public Term getLeftTerm() {
        return leftTerm;
    }

    public Term getRightTerm() {
        return rightTerm;
    }

    @Override
    public List<ASTNode> getChildren() {
        List<ASTNode> children = new LinkedList<>();
        children.add(leftTerm);
        children.add(rightTerm);
        return children;
    }
}
