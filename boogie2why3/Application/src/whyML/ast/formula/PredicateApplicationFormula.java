package whyML.ast.formula;

import whyML.ast.ASTNode;
import whyML.ast.terms.Term;
import whyML.astvisitor.ASTVisitor;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by Michael on 6/18/2015.
 */
public class PredicateApplicationFormula extends Formula {

    private String lqualid;
    /**
     *     must contain at least 1 to be valid syntax
     */
    private List<Term> terms;

    public PredicateApplicationFormula(String lqualid, List<Term> terms) {
        this.lqualid = lqualid;
        this.terms = terms;
    }

    public PredicateApplicationFormula(String lqualid, Term term) {
        this.lqualid = lqualid;
        terms = new LinkedList<>();
        terms.add(term);
    }

    public String getLqualid() {
        return lqualid;
    }

    public void setLqualid(String lqualid) {
        this.lqualid = lqualid;
    }

    public List<Term> getTerms() {
        return terms;
    }

    @Override
    public void accept(ASTVisitor visitor) {
        visitor.visit(this);
    }

    @Override
    public List<ASTNode> getChildren() {
        List<ASTNode> children = new LinkedList<>();
        children.addAll(terms);
        return children;
    }
}
