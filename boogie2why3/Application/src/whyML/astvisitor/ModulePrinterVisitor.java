package whyML.astvisitor;

import util.Log;
import whyML.ast.FormulaCase;
import whyML.ast.Label;
import whyML.ast.Module;
import whyML.ast.Param;
import whyML.ast.declerations.*;
import whyML.ast.declerations.letdeclaration.*;
import whyML.ast.expression.*;
import whyML.ast.formula.*;
import whyML.ast.formula.quantifier.FormulaBinder;
import whyML.ast.formula.quantifier.QuantifiedFormula;
import whyML.ast.formula.quantifier.Trigger;
import whyML.ast.pattern.Pattern;
import whyML.ast.pattern.TuplePattern;
import whyML.ast.pattern.VariablePattern;
import whyML.ast.spec.*;
import whyML.ast.terms.*;
import whyML.ast.types.EmptyTupleType;
import whyML.ast.types.TypeSymbol;
import whyML.ast.types.TypeTuple;
import whyML.ast.types.TypeVariable;

import java.io.IOException;
import java.nio.file.*;
import java.util.LinkedList;
import java.util.List;

/**
 * prints a WhyML module
 */
public class ModulePrinterVisitor implements ASTVisitor {

    //IMPORTANT: convention: except for Declarations, each node in the AST prints itself to the last line (i.e. doesn't
    //create a new line for itself. Each Node must create new lines and indentations for the next node, if necessary.

    private Module moduleToPrint;
    private List<String> outputText; //each string represents a line

    private int indentationLevel;

    public ModulePrinterVisitor(Module module){
        moduleToPrint = module;
        this.outputText = new LinkedList<String>();
        indentationLevel = 0;
    }

    /**
     * add indents to the beginning of a string, and return as a new string
     * @param amount how many indents to add
     * @param s the string.
     * @return same as s, but with indents infront of it.
     */
    public static String addIndents(int amount, String s){
        String res = s;
        for(int i = 0; i < amount; ++i){
            res = "\t"+res;
        }
        return res;
    }

    /**
     * indent s according to current indentation level, then add it to output text.
     * @param s the string to be added.
     */
    public void indentAndAdd(String s){
        outputText.add(addIndents(indentationLevel, s));
    }

    /**
     * Print module as a list of strings. Each String element in the list represents a new line.
     * @return
     */
    public List<String> printModuleToString(){
        indentAndAdd("module "+moduleToPrint.getName()+" "+ "\"" + moduleToPrint.getComment()+"\"");
        ++indentationLevel;
        for(Declaration d : moduleToPrint.getDeclarations()){
            d.accept(this);
        }
        --indentationLevel;
        indentAndAdd("end");
        return outputText;
    }

    /**
     * prints fileLines to fileName, if it is not null, or to console if it is null.
     * @param fileLines
     * @param fileName
     */
    public static void print(List<String> fileLines, String fileName){
        if(fileName == null){
            printToConsole(fileLines);
        }else {
            printToFile(fileLines,fileName);
        }
    }

    /**
     * print fileLines to the console. each element of the list is printed as a new line.
     * @param fileLines
     */
    public static void printToConsole(List<String> fileLines){
        for(String line: fileLines){
            System.out.println(line);
        }
    }

    /**
     * print fileLines to fileName in working directory.
     * Creates the specified file if it doesn't exist. (path starting inside working directory).
     * If the specified file already exists, nothing happens.
     * @param fileLines each line of the file
     * @param fileName Name of the new file. Can contain a path. Starts at root of working directory.
     */
    public static void printToFile(List<String> fileLines, String fileName){
        createFile(fileName);
        for(String s: fileLines){
            appendFileBytes(fileName,s+System.getProperty("line.separator"));
        }
    }

    static void appendFileBytes(String filename, String content) {
        try {
            Files.write(FileSystems.getDefault().getPath(".", filename),
                    content.getBytes(),
                    StandardOpenOption.APPEND);
        }
        catch ( IOException ioe ) {
            ioe.printStackTrace();
        }
    }

    /**
     * creates a file if none exists. (path starting inside working directory). If the specified file already exists,
     * nothing happens.
     * @param filename Name of the new file. Can contain a path. Starts at root of working directory.
     */
    static void createFile(String filename){
        try {
            Files.createFile(Paths.get(filename));
        }catch (FileAlreadyExistsException e){
            Log.error("File "+filename+" already exists, text was appended to file.");
        }catch (IOException e) {
            e.printStackTrace();
        }
    }


    @Override
    public void visit(Use use) {
        outputText.add("\t"+use.toString());
    }

    @Override
    public void visit(Axiom axiom) {
        indentAndAdd("axiom "+ axiom.getIdent()+": ");
        axiom.getFormula().accept(this);
    }

    public void visit(Lemma lemma){
        indentAndAdd("lemma "+ lemma.getIdent()+": ");
        lemma.getFormula().accept(this);
    }

    @Override
    public void visit(TypeDeclaration td) {
        String res = "type ";
        res = res.concat(td.getLident()+" ");
        for(TypeVariable tv: td.getTypeParams()){
            res = res.concat(tv.getTypeName()+" ");
        }
        if(td.getAliasType() != null){
            res = res.concat("= "+td.getAliasType().getTypeName());
        }
        indentAndAdd(res);
    }


    @Override
    public void visit(Comment comment) {
        indentAndAdd(comment.toString());
    }



    @Override
    public void visit(Empty empty) {
        outputText.add("");
    }

    @Override
    public void visit(ValGlobalVariable val) {
        String res = "val ";
        if(val.getIsGhost()){
            res = res.concat("ghost ");
        }

        res = res.concat(val.getLident()+" ");
        if(val.getLabel().getHasLabel()){
            res = res.concat(val.getLabel().toString()+"" );
        }

        res = res.concat(": "+val.getType().getTypeName());
        indentAndAdd(res);
    }

    @Override
    public void visit(ConstantDeclaration constantDeclaration) {
        String res = "constant "+constantDeclaration.getLident();
        if(constantDeclaration.getLabel().getHasLabel()){
            res = res.concat(" "+constantDeclaration.getLabel().toString());
        }
        res = res.concat(": "+constantDeclaration.getType().getTypeName());

        indentAndAdd(res);
        if(constantDeclaration.getEqualityTerm() != null){
            addToLastLine(" = ");
            //term visitor will add to last line by convenction.
            constantDeclaration.getEqualityTerm().accept(this);
        }
    }

    @Override
    public void visit(Param p) {
        String res = "(";
        if(p.getIsGhost()){
            res = res + "(ghost ";
        }
        res = res.concat(p.getName());
        if(p.getIsGhost()){
            res = res.concat("): ");
        }else{
            res = res.concat(": ");
        }

        res = res.concat(p.getType().getTypeName());
        res = res.concat(") ");
        addToLastLine(res);
    }

    @Override
    public void visit(ValAbstractFunction val) {

        String res = "val ";
        res = res + val.getName() + " ";
        //todo ghost (not needed for translation)
        if(val.getInputArguments().isEmpty()){
            res = res + "()";
        }else{
            for(Param p: val.getInputArguments()){
                res = res + "(";
                if(p.getIsGhost()){
                    res = res + "(ghost ";
                }
                res = res.concat(p.getName());
                if(p.getIsGhost()){
                    res = res.concat("): ");
                }else{
                    res = res.concat(": ");
                }

                res = res.concat(p.getType().getTypeName());
                res = res.concat(")");
            }
        }

        res = res.concat(": ");
        res = res.concat(val.getReturnType().getTypeName());

        indentAndAdd(res);
        ++indentationLevel;

        for(Spec s: val.getSpecs()){
            indentAndAdd("");
            s.accept(this);
        }

        --indentationLevel;
        indentAndAdd("");
    }

    @Override
    public void visit(Predicate predicate) {
        String res = "predicate ";
        if(predicate.getIsInfixOrPrefix()){
            res = res.concat("("+predicate.getName()+ ") ");
        }else{
            res = res.concat(predicate.getName()+ " ");
        }
        if(predicate.getLabel().getHasLabel()){
            res = res.concat(predicate.getLabel().toString()+" ");
        }
        for(Param p: predicate.getParams()){
            res = res + "(";
            res = res.concat(p.getName());
            res = res.concat(": ");
            res = res.concat(p.getType().getTypeName());
            res = res.concat(") ");
        }

        if(predicate.getOptionalFormula() != null){
            res = res.concat("= ");
            indentAndAdd(res);
            ++indentationLevel;
            predicate.getOptionalFormula().accept(this);
            --indentationLevel;
        }else{ //no formula
            indentAndAdd(res);
        }
        //TODO "with" ... (not needed for translation)
    }


    private String prefix = "_______";//TODO uses 7 underscores
    private int functionInputCounter = 0;
    private String getNextFunctionInputname(){
        return prefix+(functionInputCounter++);
    }

    @Override
    public void visit(FunctionDeclaration functionDeclaration) {
        String res = "function ";
        if(functionDeclaration.getIsInfixOrPrefix()){
            res = res.concat("("+functionDeclaration.getName()+ ") ");
        }else{
            res = res.concat(functionDeclaration.getName()+ " ");
        }
        if(functionDeclaration.getLabel().getHasLabel()){
            res = res.concat(functionDeclaration.getLabel().toString()+" ");
        }
        for(Param p: functionDeclaration.getParams()){
            res = res + "(";
            //in boogie, function names are optional.
            //this could also be added to translator instead.
            if(p.getName()!=null){
                res = res.concat(p.getName());
            }else{
                res = res.concat(getNextFunctionInputname());
            }
            res = res.concat(": ");
            res = res.concat(p.getType().getTypeName());
            res = res.concat(") ");
        }
        if(functionDeclaration.getParams().isEmpty()){
            res = res + "() ";
        }

        res = res.concat(": "+functionDeclaration.getReturnType().getTypeName()+" ");

        if(functionDeclaration.getOptionalTerm() != null){
            res = res.concat("= ");
            indentAndAdd(res);
            ++indentationLevel;
            indentAndAdd("");
            functionDeclaration.getOptionalTerm().accept(this);
            --indentationLevel;
        }else{ //no formula
            indentAndAdd(res);
        }
        //TODO "with" ...
    }

    @Override
    public void visit(NegationFormula negationFormula) {
        addToLastLine("not (");
        negationFormula.getFormula().accept(this);
        addToLastLine(")");
    }


    @Override
    public void visit(FalseFormula f) {
        addToLastLine(" false ");
    }

    @Override
    public void visit(TrueFormula t) {
        addToLastLine(" true ");
    }

    @Override
    public void visit(SymbolFormula formula) {
        addToLastLine(" "+formula.getLqualid()+" ");
    }

    @Override
    public void visit(IntegerTerm term) {
        addToLastLine(" " + term.getValue() + " ");
    }

    @Override
    public void visit(IntegerExpression integerExpression) {
        addToLastLine(" " + integerExpression.getValue() + " ");
    }

    @Override
    public void visit(ConditionalExpression conditionalExpression) {
        addToLastLine("if ");
        conditionalExpression.getCondition().accept(this);
        addToLastLine(" then (");
        ++indentationLevel;
        indentAndAdd("");
        conditionalExpression.getThenExpression().accept(this);
        --indentationLevel;
        indentAndAdd(")");
        if(conditionalExpression.getElseExpression() != null){
            addToLastLine("else( ");
            ++indentationLevel;
            indentAndAdd("");
            conditionalExpression.getElseExpression().accept(this);
            --indentationLevel;
            indentAndAdd(")");
        }
    }

    @Override
    public void visit(WhileLoopExpression expression) {
        addToLastLine("while ");
        expression.getWhileCondition().accept(this);
        addToLastLine("do ");
        ++indentationLevel;
        for(Invariant invariant: expression.getInvariants()){
            indentAndAdd("");
            invariant.accept(this);
        }
        indentAndAdd("");

        if(expression.getVariant() != null){
            expression.getVariant().accept(this);
            indentAndAdd("");
            indentAndAdd("");
        }
        expression.getLoopExpression().accept(this);
        --indentationLevel;
        indentAndAdd("done");

    }

    @Override
    public void visit(Variant variant) {
        //TODO adjust for varaint-rel once it is added.
        addToLastLine("variant{ ");
        variant.getTerms().get(0).accept(this);
        for(int i = 1; i < variant.getTerms().size(); ++i){
            addToLastLine(", ");
            variant.getTerms().get(i).accept(this);
        }
        addToLastLine("} ");
    }

    @Override
    public void visit(Handler handler) {
        addToLastLine(handler.getUqualid()+" ");
        if(handler.getPattern() != null){
            handler.getPattern().accept(this);
        }
        addToLastLine("-> ");
        handler.getExpression().accept(this);
    }

    @Override
    public void visit(ExceptionCatchingExpression expression) {
        addToLastLine("try(");
        ++indentationLevel;
        indentAndAdd("");
        expression.getExpression().accept(this);
        --indentationLevel;
        indentAndAdd(")with");
        ++indentationLevel;
        for(Handler handler:expression.getHandlers()){
            indentAndAdd("|");
            handler.accept(this);
        }
        --indentationLevel;
        indentAndAdd("end ");
    }

    @Override
    public void visit(ExceptionRaisingExpression raisingExpression) {
        addToLastLine("raise "+raisingExpression.getUqualid()+" ");

    }

    @Override
    public void visit(RealExpression realExpression) {
        addToLastLine(" " + realExpression.getValue() + " ");
    }

    @Override
    public void visit(RealTerm realTerm) {
        addToLastLine(" " + realTerm.getValue() + " ");
    }

    @Override
    public void visit(SymbolTerm symbol) {
        addToLastLine(" " + symbol.getLqualid() + " ");
    }

    @Override
    public void visit(SymbolExpression symbolExpression) {
        addToLastLine(" " + symbolExpression.getLqualid() + " ");
    }

    @Override
    public void visit(AssertionExpression assertionExpression) {
        addToLastLine(assertionExpression.getAssertionType().toString()+" ");
        if(assertionExpression.getFormula() != null){//if it's an absurd expression, there is no formula
            addToLastLine("{");
            assertionExpression.getFormula().accept(this);
            addToLastLine("}");
        }

    }

    @Override
    public void visit(SequenceExpression sequenceExpression) {
        sequenceExpression.getExp1().accept(this);
        addToLastLine(";");
        indentAndAdd("");
        sequenceExpression.getExp2().accept(this);
    }

    @Override
    public void visit(LocalBindingExpression localBindingExpression) {
        addToLastLine("let ");
        localBindingExpression.getPattern().accept(this);
        addToLastLine("= ");
        localBindingExpression.getBoundExpr().accept(this);
        addToLastLine("in ");
        indentAndAdd("");
        localBindingExpression.getNextExpr().accept(this);
    }

    public void addToLastLine(String s){
        String lastLine = outputText.get(outputText.size()-1);
        outputText.set(outputText.size()-1, lastLine + s);
    }

    @Override
    public void visit(Trigger trigger) {
        trigger.getTriggerTerms().get(0).accept(this);
        for(int i = 1; i<trigger.getTriggerTerms().size();++i){
            addToLastLine(", ");
            trigger.getTriggerTerms().get(i).accept(this);
        }
    }



    @Override
    public void visit(QuantifiedFormula qf) {
        String res = "";
        if(qf.getIsForall()){
            res = res.concat("forall ");
        }else{
            res = res.concat("exists ");
        }

        for(int i = 0; i < qf.getBinders().size(); ++i){
            FormulaBinder b = qf.getBinders().get(i);
            res = res.concat(b.toString());
            if(i+1 < qf.getBinders().size()){
                res = res.concat(", ");
            }
        }
        addToLastLine(res);
        boolean isFirstTrigger = true;
        boolean hasTriggers = !(qf.getTriggers() == null) && !(qf.getTriggers().isEmpty());
        if(hasTriggers){
            addToLastLine("[");
        }
        for(Trigger trigger: qf.getTriggers()){
            if(!isFirstTrigger){
                addToLastLine(" | ");
            }
            trigger.accept(this);
            isFirstTrigger = false;
        }
        if(hasTriggers){
            addToLastLine("]");
        }
        addToLastLine(" . ");
        ++indentationLevel;
        qf.getFormula().accept(this);
        --indentationLevel;

    }

    @Override
    public void visit(InfixFormula formula) {
        addToLastLine("((");
        formula.getLeftTerm().accept(this);
        addToLastLine(") ");
        addToLastLine(formula.getInfixOperator() + " ");
        addToLastLine("(");
        formula.getRightTerm().accept(this);
        addToLastLine(")) ");
    }


    @Override
    public void visit(InfixTerm infixTerm) {
        addToLastLine("((");
        infixTerm.getLeftTerm().accept(this);
        addToLastLine(") ");
        addToLastLine(infixTerm.getInfixOperator() +" ");
        addToLastLine("(");
        infixTerm.getRightTerm().accept(this);
        addToLastLine(")) ");
    }

    @Override
    public void visit(OldTerm oldTerm) {
        addToLastLine("(old (");
        oldTerm.getSubTerm().accept(this);
        addToLastLine("))");
    }

    @Override
    public void visit(PrefixTerm prefixTerm) {
        addToLastLine("(");
        addToLastLine(prefixTerm.getInfixOperator());
        addToLastLine("(");
        prefixTerm.getSubTerm().accept(this);
        addToLastLine(")) ");
    }

    @Override
    public void visit(InfixExpression expression) {
        addToLastLine("((");
        expression.getLeftExpression().accept(this);
        addToLastLine(") ");
        addToLastLine(expression.getInfixOperator()+" ");
        addToLastLine("(");
        expression.getRightExpression().accept(this);
        addToLastLine("))");
    }

    @Override
    public void visit(PrefixExpression prefixExpression) {
        addToLastLine("(");
        addToLastLine(prefixExpression.getPrefixOperator());
        addToLastLine("(");
        prefixExpression.getSubExpression().accept(this);
        addToLastLine(")) ");
    }

    @Override
    public void visit(BinaryFormula formula) {
        addToLastLine("((");
        formula.getLeftFormula().accept(this);
        addToLastLine(")");
        addToLastLine(formula.getOperator());
        addToLastLine("(");
        formula.getRightFormula().accept(this);
        addToLastLine(")) ");
    }

    @Override
    public void visit(ExceptionDeclaration ed) {
        indentAndAdd("exception "+ed.getUident());
    }


    @Override
    public void visit(VariablePattern variablePattern) {
        addToLastLine(variablePattern.getLident()+" ");
    }

    @Override
    public void visit(TuplePattern tuplePattern) {
        //tuplePattern contains at least 2 patterns to be valid syntax
        List<Pattern> patterns = tuplePattern.getPatterns();
        patterns.get(0).accept(this);
        for(int i = 1; i<patterns.size();++i){
            addToLastLine(", ");
            patterns.get(i).accept(this);
        }
    }

    @Override
    public void visit(TupleExpression tupleExpression) {
        addToLastLine("(");
        tupleExpression.getExpressions().get(0).accept(this);
        for(int i = 1; i<tupleExpression.getExpressions().size(); ++i){
            addToLastLine(", ");
            tupleExpression.getExpressions().get(i).accept(this);
        }
        addToLastLine(")");
    }

    @Override
    public void visit(TupleTerm tupleTerm) {
        addToLastLine("(");
        tupleTerm.getTerms().get(0).accept(this);
        for(int i = 1; i < tupleTerm.getTerms().size();++i){
            addToLastLine(", ");
            tupleTerm.getTerms().get(i).accept(this);
        }
        addToLastLine(")");
    }

    @Override
    public void visit(FieldAssignmentExpression fae) {
        fae.getFirstExpression().accept(this);
        if(!(fae.getLqualid() == null)){
            addToLastLine(".");
            addToLastLine(fae.getLqualid());
        }
        addToLastLine(" <- ");
        fae.getSecondExpression().accept(this);
    }

    @Override
    public void visit(FormulaCase formulaCase) {
        addToLastLine("\u007C"); //vertical bar �
        formulaCase.getPattern().accept(this);
        addToLastLine(" -> ");
        formulaCase.getFormula().accept(this);
    }

    @Override
    public void visit(ReturnsSpecification returnsSpecification) {
        addToLastLine("returns{");
        for(FormulaCase formulaCase  : returnsSpecification.getFormulaCases()){
            formulaCase.accept(this);
        }
        addToLastLine("}");
    }

    @Override
    public void visit(WritesSpec spec) {
        addToLastLine("writes{");
        for(int i = 0; i < spec.getTerms().size(); ++i){
            spec.getTerms().get(i).accept(this);
            if((i+1) < spec.getTerms().size()){
                addToLastLine(", ");
            }
        }
        addToLastLine("}");
    }

    @Override
    public void visit(Postcondition p) {
        addToLastLine("ensures {");
        p.getFormula().accept(this);
        addToLastLine("}");
    }



    @Override
    public void visit(Precondition p) {
        addToLastLine("requires {");
        p.getFormula().accept(this);
        addToLastLine("}");
    }

    @Override
    public void visit(Invariant inv) {
        addToLastLine("invariant {");
        inv.getFormula().accept(this);
        addToLastLine("}");
    }

    @Override
    public void visit(FunctionApplicationTerm fat) {
        addToLastLine("("+fat.getLqualid()+" ");
        for(Term t: fat.getTerms()){
            addToLastLine("(");
            t.accept(this);
            addToLastLine(") ");
        }
        if(fat.getTerms().isEmpty()){
            addToLastLine("()");
        }
        addToLastLine(") ");
    }

    @Override
    public void visit(PredicateApplicationFormula predicateApplication) {
        addToLastLine("("+predicateApplication.getLqualid()+" ");
        for(Term t: predicateApplication.getTerms()){
            addToLastLine("(");
            t.accept(this);
            addToLastLine(")");
        }
        if(predicateApplication.getTerms().isEmpty()){
            addToLastLine("()");
        }
        addToLastLine(") ");
    }

    @Override
    public void visit(FunctionApplicationExpression expression) {
        addToLastLine("(");
        expression.getFirstExpression().accept(this);
        for(Expression argument: expression.getArguments()){
            addToLastLine("(");
            argument.accept(this);
            addToLastLine(")");
        }
        if(expression.getArguments().isEmpty()){
            addToLastLine("()");
        }
        addToLastLine(")");
    }

    @Override
    public void visit(LetDeclarationBinder letDeclarationBinder) {
        if(letDeclarationBinder.getIsParam()){
            letDeclarationBinder.getParam().accept(this);
        }else{
            //TODO implement (not needed for translation)
            throw new RuntimeException("Printer not implemented for recursive let declarations with input arguments which have no" +
                    "explicit type");
        }
    }

    @Override
    public void visit(FunBody funBody) {
        for(LetDeclarationBinder binder: funBody.getBinders()){
            binder.accept(this);
        }
        if(funBody.getBinders().isEmpty()){
            addToLastLine("() ");
        }
        if(funBody.getReturnType() != null){
            addToLastLine(": ");
            addToLastLine(funBody.getReturnType().getTypeName());
        }

        ++indentationLevel;
        for(Spec spec: funBody.getSpecs()){
            indentAndAdd("");
            spec.accept(this);
        }
        --indentationLevel;
        indentAndAdd("=(");
        ++indentationLevel;
        indentAndAdd("");
        funBody.getExpr().accept(this);
        --indentationLevel;
        indentAndAdd(") ");

    }

    @Override
    public void visit(FunDefn funDefn) {
        if(funDefn.getIsGhost()){
            addToLastLine("ghost ");
        }
        addToLastLine(funDefn.getLident()+" ");
        if(funDefn.getLabel() != null && funDefn.getLabel().getHasLabel()){
            Log.error("Label in Why AST ignored: "+funDefn.getLabel());
        }
        funDefn.getFunBody().accept(this);
    }

    @Override
    public void visit(RecDefn recDefn) {
        recDefn.getFunDefns().get(0).accept(this);
        for(int i = 1; i < recDefn.getFunDefns().size(); ++i){
            addToLastLine(" with ");
            recDefn.getFunDefns().get(i).accept(this);
        }
    }

    @Override
    public void visit(RecursiveLetDeclaration recursiveLetDeclaration) {
        indentAndAdd("let rec ");
        recursiveLetDeclaration.getRecDefn().accept(this);
        indentAndAdd("");
    }

    @Override
    public void visit(LetDeclaration letDeclaration) {
        indentAndAdd("let ");
        if(letDeclaration.isGhost()){
            addToLastLine("ghost ");
        }
        addToLastLine(letDeclaration.getLident()+" ");
        letDeclaration.getLabel().accept(this);
        letDeclaration.getPgmDefn().accept(this);
        indentAndAdd("");
    }

    @Override
    public void visit(TypeSymbol typeSymbol) {
        addToLastLine(typeSymbol.getTypeName());
    }

    @Override
    public void visit(TypeTuple typeTuple) {
        addToLastLine(typeTuple.getTypeName());
    }

    @Override
    public void visit(TypeVariable typeVariable) {
        addToLastLine(typeVariable.getTypeName());
    }

    @Override
    public void visit(EmptyTupleType type) {
        addToLastLine(type.getTypeName());
    }

    @Override
    public void visit(Label label) {
        if(label.getHasLabel()){
            throw new RuntimeException("print label.");
        }
    }

    @Override
    public void visit(FormulaBinder formulaBinder) {
        addToLastLine(formulaBinder.toString());
    }
}






















