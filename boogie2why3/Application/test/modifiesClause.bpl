var v:int;
procedure p(x:int) returns(res:int);
modifies v;

procedure p2(x:int) returns (res:int)
modifies v;
{
  var i:int;
  i := 1;
  v := 3;
  call i := p(0);
  assert v == 3;//NOT verifiable
}
