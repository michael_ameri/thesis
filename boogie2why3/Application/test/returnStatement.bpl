var v: int where v > 0;


procedure p(x: int) returns (res:int)
requires x > 0;
ensures res < 4;
{
    var i: int;
    var b1, b2: bool;
    var c: int;
    assume  res < 3;
}

procedure p2(x: int) returns (res:int)
requires x >= 11;
ensures res < 4;
{
    var i: int;
    var b1, b2: bool;

    if (b1){
        assume  res < 3;
        assert res < 3;
        return;
    }else{
        assume res < 0;
        assert res < 1;
    }
}