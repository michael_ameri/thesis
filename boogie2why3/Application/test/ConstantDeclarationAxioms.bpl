type t;

const c0: t extends ;
const c1, c2: t extends ;

const unique c3, c4: t;


const c5:t extends c6, c7;
const c6, c7:t;

const c8, c9:t extends c5;