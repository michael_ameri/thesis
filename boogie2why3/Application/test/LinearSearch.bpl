// 'arr' is an "array" of size 'size'. If it contains the element 'value', its index is returned. Otherwise -1 is returned.
procedure linearSearch(arr: [int]int, size: int, value:int) returns (index: int)
	requires size >= 0;
	ensures (index == -1 || (arr[index] == value && index >= 0 && index < size));
	ensures (exists i:int :: 0 <= i && i < size && arr[i] == value) ==> (index >= 0 && index < size && arr[index] == value); //if the array contians value, then the correct index is returned.
	ensures (!(exists i:int :: 0 <= i && i < size && arr[i] == value) ==> index == -1); //if the array does not contian value, then -1 is returned.
{
	index := 0;
	while(index < size)
		invariant index <= size;
		invariant index >= 0;
		invariant (forall i:int :: 0 <= i && i < index ==> arr[i] != value);
	{
		if (arr[index] == value){ return; }
		index := index + 1;
	}
	index := -1;
}