procedure mimpl_whereClauseSimple (xin: int) returns (result: int)
ensures xin <= result;
{
  var x: int;
  x := 5;
  result := x;
  result := -1;
  assert 0 <= x;
  result := xin + 1;
}

implementation mimpl_whereClauseSimple(xin2:int) returns (r:int)
{
  assume xin2 <= 4;
  r := 5;
}

implementation mimpl_whereClauseSimple(xin2:int) returns (r:int)
{
  r := xin2;
}