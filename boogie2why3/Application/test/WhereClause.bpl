//TODO add assert statement to be verified
var global: int where global == 5;
procedure p(xin3:int where xin3<0) returns (res:int)
{
  var i:int where i > 0;
  assert i > 0;
  call i := f(0);
  assert i == 3;
  res := xin3;
  assert res < 0;
  assert global ==5;
}

procedure f(x:int) returns (res:int where res == 3);