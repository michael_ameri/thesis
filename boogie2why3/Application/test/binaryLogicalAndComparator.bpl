type a;
type c;

const x0: int;

var x12: int where x0 > 3;
var x1,x2,x3: a;
var x4,x5: c;
var x6, x7, x8: int where x7 > 5;
var x9, x10: bool where x9 && x10;
var x11: [int]bool;



axiom 1 < 2;
axiom 1 <: 1;
axiom ((1<2) ==> (x0 > 3)) && (3<4 <== 1<x0);
axiom (1>=x0) <==> (20 <= x0);
axiom (x0!=5) || (x0 == 5);