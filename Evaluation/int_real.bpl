//syntax source: https://boogie.codeplex.com/discussions/397357
procedure division() returns (res: real)
ensures res == 2.5;
ensures res == 5.0 /2;
ensures res == 5 / 2.0;
{
	res := 5 / 2; //division with this symbol automatically converts from int to real.
}

//div represents eucledian division.
procedure divisionInt() returns (res: int)
ensures res == 2;
{
	res := 5 div 2; //int division rounds down.
}

procedure divisionIntNegative() returns (res: int)
ensures res == -3;
{
	res := -5 div 2; //negative int division rounds down!
}

procedure divisionIntNegative2() returns (res: int)
ensures res == 3;
{
	res := (-5) div (-2); //Interesting result! not equal to 5 div 2!
}

procedure modInt() returns (res: int)
ensures res == 1;
ensures res == 5 mod 2;
ensures res == 5 mod -2;
ensures res == -5 mod 2;
{
	res := (-5) mod (-2); //mod ignores signs.
}

procedure conversionIntToReal() returns (res: real)
ensures res == 3.0;
{
	res := real(3); 
}

procedure conversionIRealToInt() returns (res: int)
ensures res == 3;
{
	res := int(3.0); 
}

procedure conversionIRealToInt2() returns (res: int)
ensures res == 3;
{
	res := int(3.1); 
}

procedure conversionIRealToInt3() returns (res: int)
ensures res == 3;
{
	res := int(3.9); //round down always 
}

procedure realPower() returns (res: real)
ensures res == 8.0; //NOT verifiable in boogie.
{
	res := 2.0**3.0;
}