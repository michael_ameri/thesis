function mao_f(i:int):real;

axiom 5 + 3 == 8;
axiom 5 -3 ==2;
axiom 2*2==4;

axiom mao_f(0) + mao_f(1) > mao_f(1);
axiom mao_f(0) - mao_f(1) == mao_f(2);
axiom mao_f(3) * mao_f(4) >= mao_f(7);

axiom 1.2 * 1.2 <= 2.1* 2.1;