//assignment and array access / store
var arr1: [int]bool;
var arr3: [int]int;

procedure mass_assignment1(arr2:[int]int) returns (index: int)
modifies arr1;
{
	index := 1;
	index := arr2[1];
	index := arr3[3];
	arr1[0] := true;
	arr1[arr2[arr3[index]]] := true;
	assert arr1[0];
	//arr1[1] := false;
}
