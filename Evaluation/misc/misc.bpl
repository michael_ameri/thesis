function mao_f(i:int):real;

axiom 5 + 3 == 8;
axiom 5 -3 ==2;
axiom 2*2==4;

axiom mao_f(0) + mao_f(1) > mao_f(1);
axiom mao_f(0) - mao_f(1) == mao_f(2);
axiom mao_f(3) * mao_f(4) >= mao_f(7);

axiom 1.2 * 1.2 <= 2.1* 2.1;

procedure marr_p1 (x:int) returns (res:int)
{
    var m: [int]bool;
    var m2: [int]bool;

    //formula
    assert (m[2:=true])[2];
    //expression
    m2 := m[1:=false];
    assert !(m2[1]);
}

//assignment and array access / store
var arr1: [int]bool;
var arr3: [int]int;

procedure mass_assignment1(arr2:[int]int) returns (index: int)
modifies arr1;
{
	index := 1;
	index := arr2[1];
	index := arr3[3];
	arr1[0] := true;
	arr1[arr2[arr3[index]]] := true;
	assert arr1[0];
	//arr1[1] := false;
}

var v: int where v > 0;


procedure mfreecon_p(x: int) returns (res:int)
requires x > 100;
free requires x > 5;
requires x > 101;
free requires x > 27;
ensures res < 4;
free ensures res < 10;
{
    var i: int;
    var b1, b2: bool;
    var c: int;
    assume  res < 3;
}

procedure m_free_con_p2(x: int) returns (res:int)
requires x >= 11;
ensures res < 4;
{
    var i: int;
    var b1, b2: bool;

    if (b1){
        assume  res < 3;
        assert res < 3;
        return;
    }else{
        assume res < 0;
        assert res < 1;
    }
}

function fib(n:int): int{
      if n==0 then
        0
      else
        if n==1 then
          1
         else
          fib(n-2) + fib(n-1)
}

procedure mFib()
ensures fib (8) == 21;
{}

procedure mimpl_whereClauseSimple (xin: int) returns (result: int)
ensures xin <= result;
{
  var x: int;
  x := 5;
  result := x;
  result := -1;
  assert 0 <= x;
  result := xin + 1;
}

implementation mimpl_whereClauseSimple(xin2:int) returns (r:int)
{
  assume xin2 <= 4;
  r := 5;
}

implementation mimpl_whereClauseSimple(xin2:int) returns (r:int)
{
  r := xin2;
}

procedure mp_p(x: int) returns (res:int)
requires x > 0;
ensures res < 3;
{
  assume  res < 3;
}

procedure mp_p2(x1,x2: int, y1: bool, y2: bool) returns (res1,res2:int, res3:bool, res4:int)
requires x1 > 0;
free requires x1 > 5;
ensures res1 < 3;
requires x2 < 0;
ensures res3;
{
  assume  res1 < 3;
  assume res3;
  assert x1 > 0;
}

procedure mp_p3(x,y: int)
requires x > 0;
ensures y < 3;
{
  assume  y < 3;
}


procedure mp_p4<a>(x,y: a)
ensures y <: x;
{
  assume  y <: x;
}

procedure mp_p5<a,b>(x1,x2:a, y1:b) returns (z1,z2:a, z3:int, z4,z5:b)
ensures z1 <: z2;
{
  assume  z1 <: z2;
}


//whereClauses
//whereClause for input arguments behave the same as free require clauses.
procedure mp_p6(x:int where x > 3) returns (y:int where y == 6)
ensures y > 5;
{
  assert x > 3;
  y := 6;
  assert x > 3;
}


procedure mp_p7(x:int) returns (y:int)
ensures y == 6;
{
  call y := mp_p6(0);
}

procedure mp_p8(x: int) returns (res:int)
requires x > 0;
ensures res < 3;
{
  var b:bool;
  if(*){
    res := -1;
  }else if(x > 3){
    res := -2;
  }else if(*){
    res := -3;
  }else{
    res := 2;
  }
}

var x: bool;
procedure msh_p (x: int) returns (y:int)
ensures x == y; //shadowed x of type int
{
  y := x; //shadowed x of type int
}

type aa;
type g;
type bb = int;
type cc = [int]bool;
type x = cc;
type y y1 y2;
type v v1 v2 = y v1 v2;
type w v1 v2 = y aa v2;
type z z1 z2 = [z1,z1,z1] z2;
type dd z4 z3 = y int z4;

type e e1 e2;
type f f1 f2 = e (y f1 f2) f2;

type aaa;
type bbb = int;
type c = [aaa]bool;

var x0: int where x0 > 3;
var x1,x2,x3: aaa;
var x4,x5: c;
var x6, x7, x8: int where x7 > 5;

var v1,v2: int, v3,v4: bool;

procedure mvar_p() returns (res:int)
modifies x7;
modifies x8;
requires 1 > 0;
ensures 2 < 3;
{
  assert x0 > 3;
  x7 := 1;
  assert x7 == 1;
  havoc x8;
  assert false; //verifiable because havoc x8 says assume (x7 > 5); (because of whereClause)
}