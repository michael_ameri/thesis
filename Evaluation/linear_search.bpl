// 'arr' is an "array" of size 'size'. If it contains the element 'val', its index is returned. Otherwise -1 is returned.
procedure LinearSearch(arr: [int]int, size: int, val:int) returns (index: int)
	requires size >= 0;
	ensures (index == -1 || (arr[index] == val && index >= 0 && index < size));
	ensures (exists i:int :: 0 <= i && i < size && arr[i] == val) ==> (index >= 0 && index < size && arr[index] == val); //if the array contians val, then the correct index is returned.
	ensures (!(exists i:int :: 0 <= i && i < size && arr[i] == val) ==> index == -1); //if the array does not contian val, then -1 is returned.
{
	index := 0;
	while(index < size)
		invariant index <= size;
		invariant (forall i:int :: 0 <= i && i < index ==> arr[i] != val);
	{
		if (arr[index] == val){ return; }
		index := index + 1;
	}
	index := -1;
}